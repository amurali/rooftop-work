import { app } from '../common/app.js';

var fhe = function() {

	var FullHierachicalEditable = {

		init: function() {
			var me = this;
			this.descendentEditables = [];
			this.parentEditable = undefined;
			this.edit = false;
			this.evtSubscribers = {
				  'editToggle': []
				, 'startEdit': []
				, 'stopEdit': []
				, 'save': []
				, 'redo': []
				, 'undo': []
			}

			this.on("mount", function() {
				me.findAncestorEditable( me.parent );
				if ( me.parentEditable && me.edit != me.parentEditable.edit )
				{
					me.edit = me.parentEditable.edit;
					me.update();
				}
			})
		},

		showEdit: function() {
			if ( this.parentEditable )
				return false;
			else
				return true;
		},

		findAncestorEditable: function( ancestor ) {
			if ( ancestor && ancestor.findAncestorEditable && ancestor.addDescendentEditable)
			{
				this.parentEditable = ancestor;
				ancestor.addDescendentEditable(this);
				this.update();
			}
			else if ( ancestor.parent )
			{
				this.findAncestorEditable( ancestor.parent );
			}
		},

		addDescendentEditable: function(descendent) {
			if ( this.descendentEditables.indexOf(descendent) < 0 )
			{
				this.descendentEditables.push(descendent);
			}
			descendent.edit = this.edit;
		},

		toggleEdit: function () {
			this.edit = ! this.edit;
			if ( this.edit )
				this.signal('startEdit');
			else
				this.signal('stopEdit');
			this.signal('editToggle');

			for ( var i in this.descendentEditables )
			{
				var d = this.descendentEditables[i];
				d.toggleEdit();
			}
			if ( this.parentEditable === undefined )
				this.update();
			else
				this.edit = this.parentEditable.edit;
		},

		save: function()
		{
			this.data.commit();
			this.signal('save');
			for ( var i in this.descendentEditables )
			{
				var d = this.descendentEditables[i];
				d.save();
			}
			this.toggleEdit();
		},

		redo: function()
		{
			this.data.redo();
			this.signal('redo');
			/*
			for ( var i in this.descendentEditables )
			{
				var d = this.descendentEditables[i];
				d.redo();
			}
			*/
			this.update();
		},

		undo: function()
		{
			this.data.undo();
			this.signal('undo');
			/*
			for ( var i in this.descendentEditables )
			{
				var d = this.descendentEditables[i];
				d.undo();
			}
			*/
			this.update();
		},

		signal: function(evname) {
			if ( evname in this.evtSubscribers )
			{
				for ( var i in this.evtSubscribers[evname] )
				{
					var cbk = this.evtSubscribers[evname][i];
					cbk();
				}
			}
		},

		upon: function(evname, func) {
			if ( evname in this.evtSubscribers )
			{
				this.evtSubscribers[evname].push(func);
			}
		}

	};

	return FullHierachicalEditable;
};

export default fhe();