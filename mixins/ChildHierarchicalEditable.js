import { app } from '../common/app.js';
import Dat from '../common/dat.js';
 
var che = function() {

	var ChildHierarchicalEditable = {

		init: function() {
			var me = this;
			this.parentEditable = undefined;
			this.edit = false;
			this.evtSubscribers = {
				  'editToggle': []
				, 'startEdit': []
				, 'stopEdit': []
				, 'save': []
				, 'redo': []
				, 'undo': []
			}

			this.on("mount", function() {
				me.findAncestorEditable( me.parent );
				if ( me.parentEditable && me.edit != me.parentEditable.edit )
				{
					me.edit = me.parentEditable.edit;
					me.update();
				}
			})
		},

		findAncestorEditable: function( ancestor ) {
			if ( ancestor && ancestor.findAncestorEditable && ancestor.addDescendentEditable )
			{
				this.parentEditable = ancestor;
				ancestor.addDescendentEditable(this);
			}
			else if ( ancestor.parent )
			{
				this.findAncestorEditable( ancestor.parent );
			}
		},

		toggleEdit: function () {
			if ( this.parentEditable )
				this.edit = this.parentEditable.edit;
			else
				this.edit = ! this.edit; 
			if ( this.edit )
				this.signal('startEdit');
			else
				this.signal('stopEdit');
			this.signal('editToggle');
		},

		save: function()
		{
			if ( this.data )
				this.data.commit();
			this.signal('save');
		},

		redo: function()
		{
			if ( this.data )
				this.data.redo();
			this.signal('redo');
			this.update();
		},

		undo: function()
		{
			if ( this.data )
				this.data.undo();
			this.signal('undo');
			this.update();
		},

		signal: function(evname) {
			if ( evname in this.evtSubscribers )
			{
				for ( var i in this.evtSubscribers[evname] )
				{
					var cbk = this.evtSubscribers[evname][i];
					cbk();
				}
			}
		},

		upon: function(evname, func) {
			if ( evname in this.evtSubscribers )
			{
				this.evtSubscribers[evname].push(func);
			}
		}

	};

	return ChildHierarchicalEditable;
};

export default che();