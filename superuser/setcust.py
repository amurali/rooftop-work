import cherrypy
import edc.db as DB

def PUT():
	data = cherrypy.request.json

	if cherrypy.session['superuser']:
		rows = DB.query("""
				SELECT id, name, full_name
				FROM secured_customer
				WHERE id = %s
			""", ( data['cust_id'],) )
		if len(rows) > 0:
			cherrypy.session['cust_id']   = rows[0][0]
			cherrypy.session['cust_abbr'] = rows[0][1]
			cherrypy.session['cust_name'] = rows[0][2]
			return {"message": "customer set"}

	cherrypy.response.status = 401
	return {"message": "not authorized"}

def GET():
	if cherrypy.session['superuser']:
		rows = DB.query("""
				SELECT id, name, full_name
				FROM secured_customer
				ORDER BY full_name 
			""")
		rv = []
		for r in rows:
			rv.append({
				  "id":   r[0]
				, "abbr": r[1]
				, "name": r[2]
			})
		return rv
	cherrypy.response.status = 401
	return {"message": "not authorized"}
