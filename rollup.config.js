import riot from 'rollup-plugin-riot';
import nodeResolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';

const options = {
  ext: 'riot'
}

export default { 
	input: 'main/main.js',
	context: 'this',
  plugins: [riot(options), nodeResolve(), commonjs()],
  external: ['riot'],
	output: {
    globals: {
      'riot': 'riot$1',
      'riot-observable': 'observable',
      'riot-route': 'route',
      'jquery': '$'
    },
		file: './app.js',
		format: 'iife',
		sourcemap: true,
		name: 'rtdPortal'
	},
};
