import cherrypy
import edc.db as DB
from edc.log import Log
from edc.config import GlobalConfig as cfg
import sys
import vymgmt
import time
import urllib
import simplejson as json

def getCustId():
	return (cherrypy.session['cust_id'], )

def getJoinBack():
	"""
		This should get optimized to a simple join....Need to check the execution plan
	"""
	return "id IN (SELECT shaper_class_id FROM customer_address WHERE cust_id = {})".format( cherrypy.session['cust_id'] )

"""
    Table "public.customer_address"
     Column      |  Type   | Modifiers 
-----------------+---------+-----------
 cust_id         | bigint  | not null
 address         | inet    | not null
 shaper_class_id | integer | 
 net_id          | bigint  | not null
 network         | cidr    | not null
Indexes:
    "customer_address_pkey" PRIMARY KEY, btree (address)
Foreign-key constraints:
    "customer_address_cust_id_fkey" FOREIGN KEY (cust_id) REFERENCES secured_customer(id)
    "customer_address_network_id_fk" FOREIGN KEY (net_id) REFERENCES network(id)


                                 Table "public.traffic_shaper_class"
       Column        |     Type     |                             Modifiers                             
---------------------+--------------+-------------------------------------------------------------------
 id                  | bigint       | not null default nextval('traffic_shaper_class_id_seq'::regclass)
 shaper_class_number | integer      | not null default nextval('traffic_shaper_number_seq'::regclass)
 mbps_up             | numeric(8,2) | not null
 mbps_down           | numeric(8,2) | not null
 name                | name         | 
 description         | name         | 
 burst_kb            | integer      | not null default 50
Indexes:
    "traffic_shaper_class_pkey" PRIMARY KEY, btree (id)
    "traffic_shaper_class_shaper_class_number_key" UNIQUE CONSTRAINT, btree (shaper_class_number)
Referenced by:
    TABLE "customer_address" CONSTRAINT "cust_addr_traffic_shaper_class_fk" FOREIGN KEY (shaper_class_id) REFERENCES traffic_shaper_class(id)



"""

custAddrMap = [
  { "dbname": "cust_id",		 "pos": 0, "extname": None,   		 	 "type": long, "restrict": getCustId, "insert": getCustId, "update": getCustId  }
, { "dbname": "address",		 "pos": 1, "extname": "address",   		 "type": str, "ordBy": 0, "pk": True, "pkInsertable": True }
, { "dbname": "shaper_class_id", "pos": 2, "extname": "shaper_class_id", "type": long, "parentFname": "id" }
, { "dbname": "net_id", 		 "pos": 3, "extname": "net_id",   		 "type": long, "retOnIns": True }
, { "dbname": "network", 		 "pos": 4, "extname": "network",   		 "type": str, "retOnIns": True }
, { "dbname": "bw_match_subnet", "pos": 5, "extname": "bw_match_subnet", "type": bool }
]


shaperMap = [
  { "dbname": "id", 				 "pos": 0, "extname": "id", 				 "type": long, "pk": True, "restrict": getJoinBack }
, { "dbname": "shaper_class_number", "pos": 1, "extname": "shaper_class_number", "type": long, "retOnIns": True }
, { "dbname": "mbps_up", 			 "pos": 2, "extname": "mbps_up", 			 "type": float }
, { "dbname": "mbps_down", 			 "pos": 3, "extname": "mbps_down", 			 "type": float }
, { "dbname": "name", 				 "pos": 4, "extname": "name", 				 "type": str }
, { "dbname": "description", 		 "pos": 5, "extname": "description", 		 "type": str }
, { "dbname": "burst_kb", 			 "pos": 6, "extname": "burst_kb", 			 "type": long }
]

def GET():
	try:
		m_shaper 	= TableRelMap("traffic_shaper_class", shaperMap)
		m_cust_addr = TableRelMap("customer_address",	  custAddrMap, m_shaper, "addresses")

		data = m_shaper.fetchAll()
		return data
	except Exception as e:
		last_type, last_value, exc_traceback = sys.exc_info()
		Log.tb(exc_traceback, last_type, last_value)
		raise e

def POST():
	try:
		m_shaper 	= TableRelMap("traffic_shaper_class", shaperMap)
		m_cust_addr = TableRelMap("customer_address",	  custAddrMap, m_shaper, "addresses")

		data = cherrypy.request.json

		# check if we already have a shaper_class_number
		# if so, set update flag to True so we can first delete the shaper data
		if 'shaper_class_number' in data and type( data['shaper_class_number'] ) is int:
			isUpdate = True
		else:
			isUpdate = False
		DB.execute("BEGIN;")
		m_shaper.apply(data)
		data = m_shaper.fetch( (data['id'],) )
		DB.execute("COMMIT;")

		urls = cfg.get('allocation.shaperurls').split("\n")
		okcnt=0
		for url in urls:
			resp = urllib.urlopen(url)
			rv = json.loads(resp.read())
			if not 'status' in rv:
				raise Exception("Bad response from {} ... {}".format(url, str(rv)))
			if rv['status'] != 'ok':
				raise Exception("Error on {} ... {}".format(url, str(rv)))
			okcnt+=1

		if len(urls) == okcnt:
			raise Exception("Bandwidth Changes posted")



		return data
	except Exception as e:
		last_type, last_value, exc_traceback = sys.exc_info()
		Log.tb(exc_traceback, last_type, last_value)
		DB.execute("ROLLBACK");
		raise e

def DELETE(id):
	try:
		m_shaper 	= TableRelMap("traffic_shaper_class", shaperMap)
		m_cust_addr = TableRelMap("customer_address",	  custAddrMap, m_shaper, "addresses")

		data = m_shaper.fetch((id,))
		DB.execute("BEGIN;")
		m_shaper.delete(data)
		DB.execute("COMMIT;")
		return {"message": "deleted"}
	except Exception as e:
		last_type, last_value, exc_traceback = sys.exc_info()
		Log.tb(exc_traceback, last_type, last_value)
		raise e
