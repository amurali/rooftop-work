
drop table sales_agent_plan_payments;
drop table sales_proposal_product_details;
drop table sales_proposal;
drop table sales_prospect;
drop table sales_agent_comp_plan;
drop table sales_comp_plan;
drop table sales_agent;


create table sales_agent (
	  id bigserial not null
	, primary key(id)
	, first_name character varying(30) not null
	, last_name character varying(40) not null
	, first_phone character varying(20) not null
	, second_phone character varying(20)
	, addr_line1 character varying(50) not null
	, addr_line2 character(50)
	, city character varying(25) not null
	, state character(2)
	, zip character (10)
	, tax_id character varying (13) not null
	, email_addr character varying not null
	, password character varying (128) not null 
	, active boolean not null default false
	, dt_created timestamp not null default now()
);

create table sales_comp_plan ( 
 	  id bigserial not null
 	, primary key(id)
	, plan_name character varying(25) not null
	, calc_func_name name not null
	, calc_func_args character varying not null
);

create table sales_agent_comp_plan (
	  id bigserial not null
	, agent_id bigint not null
	, foreign key(agent_id) references sales_agent(id)
	, plan_id bigint not null
	, foreign key(plan_id) references sales_comp_plan(id)
	, calc_func_args character varying
	, dt_accepted timestamp
	, dt_expires timestamp
);

create table sales_prospect (
	  id bigserial not null
	, primary key(id)
	, prospect_name character varying not null
	, contact_name character varying not null
	, addr_line1 character varying not null
	, addr_line2 character varying
	, city character varying not null
	, state char(2) not null
	, zip character (10) not null
	, email_addr character varying not null
	, phone_number char(10) not null
	, dt_created timestamp not null default now()
	, created_by_agent bigint not null
	, holding_agent bigint not null
	, foreign key (created_by_agent) references sales_agent(id)
	, foreign key (holding_agent) references sales_agent(id)
);

create unique index sales_prospect_name_idx
ON sales_prospect (lower(prospect_name));

create unique index sales_prospect_addr_idx
ON sales_prospect (lower(addr_line1), lower(addr_line2), lower(substr(zip, 5)));

create table sales_proposal (
	  id bigserial not null
	, primary key (id)
	, term_months int not null default 24
	, agent_id bigint not null
	, foreign key (agent_id) references sales_agent(id)
	, prospect_id bigint not null
	, foreign key (prospect_id) references sales_prospect(id)
	, discount int not null default 0
	, check (discount >= 0 AND discount <= 100)
	, dt_created timestamp not null default now()
	, link_code character (32) not null default md5(''||random()||now())
	, unique(link_code)
	, dt_sent timestamp
	, dt_valid_until timestamp
	, dt_accepted timestamp
	, dt_declined timestamp
	, active boolean not null default TRUE
);

create table sales_proposal_product_details (
	    id bigserial not null
	  , primary key(id)
	  , proposal_id bigint not null
	  , foreign key (proposal_id) references sales_proposal(id)
	  , product_detail_id bigint not null
	  , foreign key (product_detail_id) references product_detail_pricing(id)
	  , term integer not null
	  , price numeric(14,3)
	  , quantity int not null default 1
);


create table sales_agent_plan_payments (
	  id bigserial not null
	, primary key (id)
	, agent_id bigint not null
	, foreign key (agent_id) references sales_agent (id)
	, plan_id bigint not null
	, proposal_id bigint not null
	, foreign key (proposal_id) references sales_proposal(id)
	, foreign key (plan_id) references sales_comp_plan (id)
	, booked boolean not null default false
	, dt_due timestamp not null
	, amount numeric(10,3) not null
);
