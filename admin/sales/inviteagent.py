import cherrypy
import edc.db as DB
from uuid import uuid4

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.parser import Parser
import smtplib
from jinja2 import Template, Environment
from edc.config import GlobalConfig as config
from edc.log import Log
import simplejson as json
import ssl

def getMessageContent(enrollUrl):
	html = ""
	text = ""


	textTemplate = """
Please copy and paste the following link into a web-browser to complete your account activation:

{{ enrollUrl }}

Please note: this link is only valid for 48 hours, and for a single use. The enrollment process should only take a few minutes.

Thank You!
		"""

	htmlTemplate = """
<!DOCTYPE html>
<HTML>
<HEAD>
<title>
	Rooftop Data Sales Agent Invitation
</title>
<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
	background-color: #ACDEFF;
	color: #02022A;
}
</style>
</HEAD>
<BODY>
Please click on <a href="{{ enrollUrl }}">this</a> link to enroll as a sales agent with Rooftop Data.
<br>
<br>
Alternately, please cut and paste the following URL into the address bar of a web browser to complete your enrollment:
<br>
{{ enrollUrl }}
<br>
Please note: this link is only valid for 48 hours, and for a single use. The enrollment process should only take a few minutes.
<br>
</BODY>
</HTML>
		"""

	env = Environment()
	#env.filters['snd'] = tpl_repl_none_dash
	#env.filters['sne'] = tpl_repl_none_empty_str

	textTempl = env.from_string(textTemplate)
	htmlTempl = env.from_string(htmlTemplate)

	text = textTempl.render(enrollUrl=enrollUrl)
	html = htmlTempl.render(enrollUrl=enrollUrl)

	return (text, html)



def sendRegistration(sendTo, enrollUrl):
	(text, html) = getMessageContent(enrollUrl)

	msg = MIMEMultipart('alternative')
	msg['Subject'] = "Rooftop Data Sales Agent Enrollment link"
	msg['From'] = config.get('email.sender')
	msg['To'] = sendTo
	part1 = MIMEText(text, 'plain')
	part2 = MIMEText(html, 'html')
	msg.attach(part1)
	msg.attach(part2)

	try:
		smtpout = smtplib.SMTP_SSL(config.get('email.server'))
	except ssl.SSLError:
		smtpout = smtplib.SMTP(config.get('email.server'))
	
	smtpout.login(config.get('email.login'), config.get('email.password'))
	rv = smtpout.sendmail(config.get('email.sender'), sendTo, msg.as_string())
	for k in rv.keys():
		Log.notice("%s => %s" % (k, rv[k]))
	rv = smtpout.quit()
	if rv is not None:
		Log.notice("SMTP Quit returned: %s" % str(rv))


def POST():
	data = cherrypy.request.json
	
	Log.info("Registration requested for email: %s" % data['email'])

	qry = """
		INSERT INTO portal.link_codes (path, data, expires, max_uses)
		VALUES ('/#/enrollagent/', %s, now() + '48 hours'::interval, 1)
		RETURNING code;
	"""

	row = DB.query(qry, ( json.dumps(data), ) )
	code = row[0][0]

	enrollUrl = '{}/api/common/link?code={}'.format(config.get('http.baseurl'), code)
	sendRegistration(data['email'], enrollUrl)
	return { "message": "Enrollment email sent" }
