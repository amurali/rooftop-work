import cherrypy
import edc.db as DB
from edc.log import Log
import sys

"""
                             Table "public.network"
    Column    |  Type   |                       Modifiers                       
--------------+---------+-------------------------------------------------------
 id           | bigint  | not null default nextval('networks_id_seq'::regclass)
 name         | name    | not null
 description  | text    | 
 address      | cidr    | not null
 discoverable | boolean | not null default true
 dom_id       | bigint  | not null default 1
Indexes:
    "networks_pkey" PRIMARY KEY, btree (id)
    "address_uk" UNIQUE CONSTRAINT, btree (address)
Foreign-key constraints:
    "network_domain_id_fk" FOREIGN KEY (dom_id) REFERENCES domain(id)

"""

networksMap = [
	  { "dbname": "id",				"pos": 0, "extname": "id",			"type": int, "pk": True}
	, { "dbname": "name",			"pos": 1, "extname": "name",      	"type": str }
	, { "dbname": "description", 	"pos": 2, "extname": "description",	"type": str }
	, { "dbname": "address", 		"pos": 3, "extname": "address", 	"type": str, "ordBy": 0 }
]

def GET():
	try:
		m_network 		= TableRelMap("network", 		networksMap)

		data = m_network.fetchAll()

		"""
			Now that we have all the networks, populate each networks addresses field
			with discovered, resource and customer addresses, tagged with each of the applicable
			tags


		"""

		for r in data:
			qry = """
			SELECT regexp_replace(host(a.addr)::text, '.*[.:]([0-9a-f]+$)', '\\1') AS ip                                                                                                                                                          
			, array_to_json(array_agg(a.tp)) AS labels
			FROM (
				SELECT host(ip)::inet AS addr    , 'DISC' as tp FROM discovered_address WHERE ip << (select address from network where id = %d)
				UNION  SELECT host(ip)::inet     , 'RES'  as tp FROM resource_address   WHERE net_id = %d
				UNION  SELECT host(address)::inet, 'CUST' as tp FROM customer_address   WHERE net_id = %d
			) AS a
			GROUP BY ip; """ % ( r['id'], r['id'], r['id'])
			Log.debug("Running query: {}".format(qry))
			rows = DB.query(qry)
			adds = []
			for i in rows:
				adds.append({
					"ip": i[0]
					, "labels": i[1]
				})
			r['addresses'] = adds
		return data
	except Exception as e:
		last_type, last_value, exc_traceback = sys.exc_info()
		Log.tb(exc_traceback, last_type, last_value)
		raise e

