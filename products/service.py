import cherrypy
import edc.db as DB
from edc.log import Log
import sys

def getCustId():
	return (cherrypy.session['cust_id'], )

productDetailPriceRecurringPath = [
 	{ "table": "product_detail_pricing", 	"alias": "pp",	"fk": "pricing_id",		"pk": "id", "jmethod": "LEFT OUTER JOIN" }
]

productDetailPriceMap = [
	  { "dbname": "id", 					"pos": 0, "extname": "id", 			"type": long, "pk": True }
	, { "dbname": "customer_detail_id", 	"pos": 1, "extname": "detail_id", 	"type": long, "parentFname": "id" }
	, { "dbname": "period", 				"pos": 2, "extname": "period", 		"type": str }
	, { "dbname": "term", 					"pos": 3, "extname": "term", 		"type": long }
	, { "dbname": "price", 					"pos": 4, "extname": "price", 		"type": str }
	, { "dbname": "pricing_id", 			"pos": 5, "extname": "pricing_id", 	"type": long }
	, { "dbname": "recurring", 				"pos": 6, "extname": "recurring", 	"type": bool, "path": productDetailPriceRecurringPath}
]

productDetailMap = [
  { "dbname": "id", 					"pos": 0, "extname": "id", 			"type":  long, "pk": True  }
, { "dbname": "customer_product_id", 	"pos": 1, "extname": "product_id", 	"type":  long, "parentFname": "id" }
, { "dbname": "name", 					"pos": 2, "extname": "name", 		"type":  str, "ordBy": 0 }
, { "dbname": "description",			"pos": 3, "extname": "description", "type":  str }
, { "dbname": "quantity", 				"pos": 4, "extname": "quantity", 	"type":  long }
, { "dbname": "product_detail_id",	 	"pos": 5, "extname": "detail_id", 	"type":  long }
, { "dbname": "cust_id", 				"pos": 6, "extname": None,			"type": long, "restrict": getCustId, "insert": getCustId, "update": getCustId }

]

"""
	The way a path works, is that the field name in question, in this case: recurring, actually
	exists in the last table in the path. When a path'd field is requested the mapping engine
	will join all the tables in the path as specified by the fk and pk's. The path is expected not to 
	result in more than one row being returned. To aid in that, restrict clauses can be added (To be implemented....)
"""
productPriceRecurringPath = [
 	{ "table": "product_pricing", 			"alias": "pp",	"fk": "pricing_id",				"pk": "id", "jmethod": "LEFT OUTER JOIN" }
]

productPriceMap = [
  {"dbname": "id", 					"pos": 0, "extname": "id", 			"type": long, "pk": True }
, {"dbname": "customer_product_id", "pos": 1, "extname": "product_id", 	"type": long, "parentFname": "id" }
, {"dbname": "pricing_id", 			"pos": 2, "extname": "pricing_id", 	"type": long }
, {"dbname": "period", 				"pos": 3, "extname": "period", 		"type": str }
, {"dbname": "term", 				"pos": 4, "extname": "term", 		"type": long }
, {"dbname": "price", 				"pos": 5, "extname": "price", 		"type": str }
, {"dbname": "recurring", 			"pos": 6, "extname": "recurring", 	"type": bool, "path": productPriceRecurringPath}
# Need to be able to pull in details from the product_price (namely recurring ....)


]

productMap =		 [
  {"dbname": "id", 				"pos": 0, "extname": "id", 				"type": long, "pk": True }
, {"dbname": "name", 			"pos": 1, "extname": "name", 			"type": str, "ordBy": 0 }
, {"dbname": "description", 	"pos": 2, "extname": "description", 	"type": str }
, {"dbname": "product_id", 		"pos": 3, "extname": "product_id", 		"type": long }
, {"dbname": "type_id", 		"pos": 4, "extname": "type_id", 		"type": long }
, {"dbname": "valid_through", 	"pos": 5, "extname": "valid_through", 	"type": str }
, {"dbname": "quote", 			"pos": 6, "extname": "quote", 			"type": bool }
, {"dbname": "purchased", 		"pos": 7, "extname": "purchased", 		"type": bool }
, {"dbname": "cust_id", 		"pos": 8, "extname": None,				"type": long, "restrict": getCustId, "insert": getCustId, "update": getCustId }
]

def GET(id=None):
	try:
		m_product 		= TableRelMap("customer_product_header", 			productMap)
		m_detail 		= TableRelMap("customer_product_detail",			productDetailMap, 		 m_product, "details")
		m_price   		= TableRelMap("customer_product_pricing",			productPriceMap, 		 m_product, "pricing")
		m_detail_price	= TableRelMap("customer_product_detail_pricing",	productDetailPriceMap, 	 m_detail, 	"pricing")

		if id is not None:
			data = m_product.fetch( (id,) )
		else:
			data = m_product.fetchAll()
		return data
	except Exception as e:
		last_type, last_value, exc_traceback = sys.exc_info()
		Log.tb(exc_traceback, last_type, last_value)
		raise e


def POST():
	try:
		m_product 		= TableRelMap("customer_product_header", 			productMap)
		m_detail 		= TableRelMap("customer_product_detail",			productDetailMap, 		 m_product, "details")
		m_price   		= TableRelMap("customer_product_pricing",			productPriceMap, 		 m_product, "pricing")
		m_detail_price	= TableRelMap("customer_product_detail_pricing",	productDetailPriceMap, 	 m_detail, 	"pricing")

		data = cherrypy.request.json
		DB.execute("BEGIN;")
		m_product.apply(data)
		DB.execute("COMMIT;")
		return data
	except Exception as e:
		last_type, last_value, exc_traceback = sys.exc_info()
		Log.tb(exc_traceback, last_type, last_value)
		raise e

def DELETE(id):
	try:
		m_product 		= TableRelMap("customer_product_header", 			productMap)
		m_detail 		= TableRelMap("customer_product_detail",			productDetailMap, 		 m_product, "details")
		m_price   		= TableRelMap("customer_product_pricing",			productPriceMap, 		 m_product, "pricing")
		m_detail_price	= TableRelMap("customer_product_detail_pricing",	productDetailPriceMap, 	 m_detail, 	"pricing")

		data = m_product.fetch((id,))
		DB.execute("BEGIN;")
		m_product.delete(data)
		DB.execute("COMMIT;")
		return {"message": "deleted"}
	except Exception as e:
		last_type, last_value, exc_traceback = sys.exc_info()
		Log.tb(exc_traceback, last_type, last_value)
		raise e
