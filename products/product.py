import cherrypy
import edc.db as DB
from edc.log import Log
import sys

productDetailPriceMap = [
	  { "dbname": "id", 		"pos": 0, "extname": "id", 			"type": int, "pk": True }
	, { "dbname": "detail_id", 	"pos": 1, "extname": "detail_id", 	"type": int, "parentFname": "id" }
	, { "dbname": "recurring", 	"pos": 2, "extname": "recurring", 	"type": bool, "ordBy": 0 }
	, { "dbname": "period", 	"pos": 3, "extname": "period", 		"type": str }
	, { "dbname": "term", 		"pos": 4, "extname": "term", 		"type": int }
	, { "dbname": "min_term", 	"pos": 5, "extname": "min_term", 	"type": int }
	, { "dbname": "max_term", 	"pos": 6, "extname": "max_term", 	"type": int }
	, { "dbname": "price", 		"pos": 7, "extname": "price", 		"type": str }
	, { "dbname": "min_price", 	"pos": 8, "extname": "min_price", 	"type": str }
	, { "dbname": "per_n", 		"pos": 9, "extname": "per_n", 		"type": int }

]

productDetailMap = [
  { "dbname": "id", 			"pos": 0, "extname": "id", 			"type":  int, "pk": True  }
, { "dbname": "product_id", 	"pos": 1, "extname": "product_id", 	"type":  int, "parentFname": "id" }
, { "dbname": "name", 			"pos": 2, "extname": "name", 		"type":  str, "ordBy": 0 }
, { "dbname": "description",	"pos": 3, "extname": "description", "type":  str }
, { "dbname": "quantity", 		"pos": 4, "extname": "quantity", 	"type":  int }
, { "dbname": "units", 			"pos": 5, "extname": "units", 		"type":  str }
, { "dbname": "adjustable", 	"pos": 6, "extname": "adjustable", 	"type":  bool }
, { "dbname": "metered", 		"pos": 7, "extname": "metered", 	"type":  bool }

]

productPriceMap = [
  {"dbname": "id", 			"pos": 0, "extname": "id", 			"type": int, "pk": True }
, {"dbname": "product_id", 	"pos": 1, "extname": "product_id", 	"type": int, "parentFname": "id" }
, {"dbname": "recurring", 	"pos": 2, "extname": "recurring", 	"type": bool, "ordBy": 0 }
, {"dbname": "period", 		"pos": 3, "extname": "period", 		"type": str }
, {"dbname": "term", 		"pos": 4, "extname": "term", 		"type": int }
, {"dbname": "min_term", 	"pos": 5, "extname": "min_term", 	"type": int }
, {"dbname": "max_term", 	"pos": 6, "extname": "max_term", 	"type": int }
, {"dbname": "price", 		"pos": 7, "extname": "price", 		"type": str }
, {"dbname": "min_price", 	"pos": 8, "extname": "min_price", 	"type": str }

]

productMap = [
  {"dbname": "id", 				"pos": 0, "extname": "id", 				"type": int, "pk": True }
, {"dbname": "name", 			"pos": 1, "extname": "name", 			"type": str, "ordBy": 0 }
, {"dbname": "description", 	"pos": 2, "extname": "description", 	"type": str }
, {"dbname": "type_id", 		"pos": 3, "extname": "type_id", 		"type": int }
, {"dbname": "valid_through", 	"pos": 4, "extname": "valid_through", 	"type": str }

]

def GET(id=None):
	try:
		m_product 		= TableRelMap("product_header", 		productMap)
		m_detail 		= TableRelMap("product_detail",			productDetailMap, 		 m_product, "details")
		m_price   		= TableRelMap("product_pricing",		productPriceMap, 		 m_product, "pricing")
		m_detail_price	= TableRelMap("product_detail_pricing",	productDetailPriceMap, 	 m_detail, 	"pricing")

		if id is not None:
			data = m_product.fetch( (id,) )
		else:
			data = m_product.fetchAll()
		return data
	except Exception as e:
		last_type, last_value, exc_traceback = sys.exc_info()
		Log.tb(exc_traceback, last_type, last_value)
		raise e


def POST():
	try:
		m_product 		= TableRelMap("product_header", 		productMap)
		m_price   		= TableRelMap("product_pricing",		productPriceMap, 		 m_product, "pricing")
		m_detail 		= TableRelMap("product_detail",			productDetailMap, 		 m_product, "details")
		m_detail_price	= TableRelMap("product_detail_pricing",	productDetailPriceMap, 	 m_detail, 	"pricing")

		data = cherrypy.request.json
		DB.execute("BEGIN;")
		m_product.apply(data)
		DB.execute("COMMIT;")
		return data
	except Exception as e:
		last_type, last_value, exc_traceback = sys.exc_info()
		Log.tb(exc_traceback, last_type, last_value)
		raise e

def DELETE(id):
	try:
		m_product 		= TableRelMap("product_header", 		productMap)
		m_price   		= TableRelMap("product_pricing",		productPriceMap, 		 m_product, "pricing")
		m_detail 		= TableRelMap("product_detail",			productDetailMap, 		 m_product, "details")
		m_detail_price	= TableRelMap("product_detail_pricing",	productDetailPriceMap, 	 m_detail, 	"pricing")

		data = m_product.fetch((id,))
		DB.execute("BEGIN;")
		m_product.delete(data)
		DB.execute("COMMIT;")
		return {"message": "deleted"}
	except Exception as e:
		last_type, last_value, exc_traceback = sys.exc_info()
		Log.tb(exc_traceback, last_type, last_value)
		raise e
