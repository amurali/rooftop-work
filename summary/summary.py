import cherrypy
from uuid import uuid4
import edc.db as DB
from edc.log import Log

def GET():
    print("Login.GET called")
    pass

def POST():
    print("summary.summary.POST called")
    data = cherrypy.request.json
     
    # Since we are not expecting any results from this insertion, simply use the execute method from DB
    if 'name' in data and 'email' in data and 'company' in data:
        DB.execute("""
                INSERT INTO portal.user_summary_info (username, email, company)
                VALUES (%s, %s, %s)
            """, (data['name'], data['email'], data['company'] ))
        print("Information inserted into table!")
        return { 'message': 'Insertion successful'}
    
    print("Insertion failed!")
    cherrypy.lib.sessions.expire()
    cherrypy.response.status = 503 # Following the HTTP Rest API error codes, this should throw 503 - Service Unvailable (probably)
    return { 'message': 'Failed attempt'}