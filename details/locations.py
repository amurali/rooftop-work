import cherrypy
import edc.db as DB

"""
        View "public.readable_location"
	   Column   |         Type          | Modifiers 
	------------+-----------------------+-----------
	 id         | bigint                | 
	 lat_long   | point                 | 
	 name       | text                  | 
	 building   | text                  | 
	 address_l1 | text                  | 
	 address_l2 | text                  | 
	 city       | text                  | 
	 state      | character(2)          | 
	 zip        | character varying(10) | 
	 floor      | character varying(9)  | 
	 room       | character varying(25) | 
	 rack       | character varying(25) | 
	 rack_pos   | character varying(25) | 


"""

def GET():
	rows = DB.query("SELECT * FROM readable_location AS l JOIN customer_locations AS c ON l.id = c.location_id WHERE c.cust_id = %s", ( cherrypy.session['cust_id'], )  )
	rv = []
	for r in rows:
		rv.append({
			  "id":         r[0]
			, "lat_long":   r[1]
			, "name":       r[2]
			, "building":   r[3]
			, "address_l1": r[4]
			, "address_l2": r[5]
			, "city":       r[6]
			, "state":      r[7]
			, "zip":        r[8]
			, "floor":      r[9]
			, "room":       r[10]
			, "rack":       r[11]
			, "rack_pos":   r[12]
			})

	return rv