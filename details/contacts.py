import cherrypy
import edc.db as DB

'''

                             Table "public.customer_contact"
     Column      |  Type  |                           Modifiers                           
-----------------+--------+---------------------------------------------------------------
 id              | bigint | not null default nextval('customer_contact_id_seq'::regclass)
 customer_id     | bigint | not null
 contact_type_id | bigint | not null
 first_name      | name   | not null
 last_name       | name   | not null
 nickname        | name   | 


                                   Table "public.customer_contact_email"
   Column   |          Type          |                              Modifiers                              
------------+------------------------+---------------------------------------------------------------------
 id         | bigint                 | not null default nextval('customer_contact_email_id_seq'::regclass)
 contact_id | bigint                 | not null
 email_type | bigint                 | not null
 email_addr | character varying(250) | not null


                                       Table "public.customer_contact_phone"
       Column        |         Type          |                              Modifiers                              
---------------------+-----------------------+---------------------------------------------------------------------
 id                  | bigint                | not null default nextval('customer_contact_phone_id_seq'::regclass)
 contact_id          | bigint                | not null
 phone_type          | bigint                | not null
 phone_number        | character varying(25) | not null
 extension           | character varying(15) | 
 first_callable_hour | smallint              | not null default 0
 last_callable_hour  | smallint              | not null default 23
 is_mobile           | boolean               | not null default false
 send_sms            | boolean               | not null default false
 carrier_id          | bigint                | 


                                     Table "public.customer_contact_address"
     Column     |          Type          |                               Modifiers                               
----------------+------------------------+-----------------------------------------------------------------------
 id             | bigint                 | not null default nextval('customer_contact_address_id_seq'::regclass)
 contact_id     | bigint                 | not null
 address_type   | bigint                 | not null
 addr_line1     | character varying(250) | not null
 addr_line2     | character varying(250) | 
 city           | name                   | 
 state_province | name                   | 
 postal_code    | character varying(25)  | 
 country        | character varying(120) | 




'''

def GET():
	'''
		First, get the contacts, then for each contact, get the emails, phones and addresses
	'''
	contacts = []
	rows = DB.query("""
			SELECT id, first_name, last_name, nickname, contact_type_id, customer_id
			FROM customer_contact 
			WHERE customer_id = %s
		""", ( cherrypy.session['cust_id'], ))
	for r in rows:
		contact = {
			"id"          : r[0]
			, "first_name": r[1]
			, "last_name" : r[2]
			, "nickname"  : r[3]
			, "type"      : r[4]
			, "cust_id"   : r[5]
			, "emails"    : list()
			, "phones"    : list()
			, "addresses" : list()
			}

		rows2 = DB.query("""
				SELECT id, email_type, email_addr
				FROM customer_contact_email
				WHERE contact_id = %s
			""", (contact['id'],))
		for r in rows2:
			contact['emails'].append({
					  "id"    : r[0]
					, "type"  : r[1]
					, "email" : r[2] 
				})

		rows2 = DB.query("""
				SELECT id, phone_type, phone_number, extension
				FROM customer_contact_phone
				WHERE contact_id = %s
			""", (contact['id'],))
		for r in rows2:
			contact['phones'].append({
					  "id"    : r[0]
					, "type"  : r[1]
					, "phone" : r[2] 
					, "ext"   : r[3]
				})


		rows2 = DB.query("""
				SELECT id, address_type, addr_line1, addr_line2
				, city, state_province, postal_code, country
				FROM customer_contact_address
				WHERE contact_id = %s
			""", (contact['id'],))
		for r in rows2:
			contact['addresses'].append({
					  "id"             : r[0]
					, "type"           : r[1]
					, "address_l1"     : r[2] 
					, "address_l2"     : r[3] 
					, "city"           : r[4] 
					, "state_province" : r[5] 
					, "postal_code"    : r[6] 
					, "country"        : r[7] 
				})


		contacts.append(contact)
	return contacts