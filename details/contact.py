import cherrypy
import edc.db as DB
from edc.log import Log
import sys
# from ..common.mapper import DbExtMap, TableRelMap .... These have been hacked into the app by treating them as builtins....

emailMap = [
	  { "dbname": "id",         "pos": 0, "extname": "id",         "type": int, "pk": True}
	, { "dbname": "email_addr", "pos": 1, "extname": "email",      "type": str, "ordBy": 0}
	, { "dbname": "email_type", "pos": 2, "extname": "type",       "type": int}
	, { "dbname": "contact_id", "pos": 3, "extname": "contact_id", "type": int, "parentFname": "id"}
]

phoneMap = [
	  { "dbname": "id",           "pos": 0, "extname": "id",         "type": int, "pk": True}
	, { "dbname": "phone_number", "pos": 1, "extname": "phone",      "type": str, "ordBy": 0}
	, { "dbname": "extension",    "pos": 2, "extname": "ext",        "type": str}
	, { "dbname": "phone_type",   "pos": 3, "extname": "type",       "type": int}
	, { "dbname": "contact_id",   "pos": 4, "extname": "contact_id", "type": int, "parentFname": "id"}
]

addressMap = [
	  { "dbname": "id",             "pos": 0, "extname": "id",             "type": int, "pk": True}
	, { "dbname": "addr_line1",     "pos": 1, "extname": "address_l1",     "type": str, "ordBy": 0}
	, { "dbname": "addr_line2",     "pos": 2, "extname": "address_l2",     "type": str}
	, { "dbname": "city",           "pos": 3, "extname": "city",           "type": str}
	, { "dbname": "state_province", "pos": 4, "extname": "state_province", "type": str}
	, { "dbname": "postal_code",    "pos": 5, "extname": "postal_code",    "type": str}
	, { "dbname": "country",        "pos": 6, "extname": "country",        "type": str}
	, { "dbname": "address_type",   "pos": 7, "extname": "type",           "type": int}
	, { "dbname": "contact_id",     "pos": 8, "extname": "contact_id",     "type": int, "parentFname": "id"}
]

contactMap = [
	  { "dbname": "id",              "pos": 0, "extname": "id",             "type": int, "pk": True }
	, { "dbname": "first_name",      "pos": 1, "extname": "first_name",     "type": str, "ordBy": 1}
	, { "dbname": "last_name",       "pos": 2, "extname": "last_name",      "type": str, "ordBy": 0}
	, { "dbname": "nickname",        "pos": 3, "extname": "nickname",       "type": str}
	, { "dbname": "contact_type_id", "pos": 4, "extname": "type",           "type": int}
	, { "dbname": "customer_id",     "pos": 5, "extname": "cust_id",        "type": int}
]



def POST():

	try:
		# these are in the functions specifically not to be module level variables (and thereby persistant...)
		m_contact = TableRelMap("customer_contact",         contactMap)
		m_email   = TableRelMap("customer_contact_email",   emailMap,   m_contact, 'emails')
		m_phone   = TableRelMap("customer_contact_phone",   phoneMap,   m_contact, 'phones')
		m_address = TableRelMap("customer_contact_address", addressMap, m_contact, 'addresses')

		data = cherrypy.request.json
		DB.execute("BEGIN;")
		m_contact.apply(data)
		DB.execute("COMMIT;")
		return data
	except Exception as e:
		last_type, last_value, exc_traceback = sys.exc_info()
		Log.tb(exc_traceback, last_type, last_value)
		raise e

def DELETE(id):
	"""
		Cascade delete any dependent objects (according to the mapping)
		and the top level contact as well.
	"""
	try:
		# these are in the functions specifically not to be module level variables (and thereby persistant...)
		m_contact = TableRelMap("customer_contact",         contactMap)
		m_email   = TableRelMap("customer_contact_email",   emailMap,   m_contact, 'emails')
		m_phone   = TableRelMap("customer_contact_phone",   phoneMap,   m_contact, 'phones')
		m_address = TableRelMap("customer_contact_address", addressMap, m_contact, 'addresses')

		data = m_contact.fetch((id,))
		DB.execute("BEGIN;")
		m_contact.delete(data)
		DB.execute("COMMIT;")
		return {"message": "deleted"}
	except Exception as e:
		last_type, last_value, exc_traceback = sys.exc_info()
		Log.tb(exc_traceback, last_type, last_value)
		raise e
