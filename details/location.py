import cherrypy
import edc.db as DB
import re

def POST():
	'''
		This API is not RESTful, but we'll use POST to CREATE new records, We'll return
		the ID's to the caller
	
		Detect matches with existing location sub-entities
	'''
	data = cherrypy.request.json
	
	re.UNICODE = True
	rx = re.compile('[^\w\.,-]+')
	
	# scrub all the fields in the data
	for f in data:
		if type(data[f]) == str:
			data[f] = rx.sub(' ', data[f]).strip()
			if len(data[f]) == 0:
				data[f] = None

	# Check for the building by address l1 and zip
	rows = DB.query("""
			SELECT id, name, address_l1, city, state, zip, lat_long FROM building WHERE lower(address_l1) = lower(%s) AND lower(zip) = lower(%s)
		""", ( data['address_l1'], data['zip'] ) )
	DB.execute("BEGIN;")
	if len(rows) > 0:
		# there is already an entry at that address and zip, so we'll use that instead
		bldg_id            = rows[0][0]
		data['building']   = rows[0][1]
		data['address_l1'] = rows[0][2]
		data['city']       = rows[0][3]
		data['state']      = rows[0][4]
		data['zip']        = rows[0][5]
		data['lat_long']   = rows[0][6]
	else:
		# this building does not exist
		rows = DB.query("""
			INSERT INTO building (
				  name
				, address_l1
				, city
				, state
				, zip
				, lat_long
			) VALUES (
				  %s
				, %s
				, %s
				, %s
				, %s
				, %s::point
			) RETURNING id;""", ( data['building'], data['address_l1'], data['city'], data['state'], data['zip'], data['lat_long']  ) )
		bldg_id = rows[0][0]

	# Check for the floor
	rows = DB.query("""SELECT id, name FROM floor WHERE building_id = %s AND lower(name) = lower(%s)""", ( bldg_id, data['floor'] ))
	if len(rows) > 0:
		floor_id      = rows[0][0]
		data['floor'] = rows[0][1]
	else:
		rows = DB.query("""
				INSERT INTO floor (
					  name
					, building_id
				) VALUES (
					  %s
					, %s
				) RETURNING id
			""", ( data['floor'], bldg_id ))
		floor_id = rows[0][0]

	rows = DB.query("""INSERT INTO location (building_id, floor_id) VALUES (%s, %s) RETURNING id""", (bldg_id, floor_id))
	loc_id = rows[0][0]

	DB.execute("""INSERT INTO customer_locations (cust_id, location_id) VALUES (%s, %s)""", (cherrypy.session['cust_id'], loc_id) )
	DB.execute("COMMIT;")
	data['id'] = loc_id

	return data




def PUT():
	'''
		This API is not RESTful but we'll use PUT to UPDATE existing 
	'''
	data = cherrypy.request.json
	
	re.UNICODE = True
	rx = re.compile('[^\w\.,-]+')
	
	# scrub all the fields in the data
	for f in data:
		if type(data[f]) == str:
			data[f] = rx.sub(' ', data[f]).strip()
			if len(data[f]) == 0:
				data[f] = None

	rows = DB.query("""SELECT building_id, floor_id FROM location WHERE id = %s""", (data['id'], ) )
	bldg_id  = rows[0][0]
	floor_id = rows[0][1]

	DB.execute("BEGIN;")

	DB.execute("""
			UPDATE building SET address_l1 = %s, city = %s, state = %s, zip = %s, lat_long = %s::point WHERE id = %s
		""", (data['address_l1'], data['city'], data['state'], data['zip'], data['lat_long'], bldg_id ) )
	DB.execute("""
			UPDATE floor SET name = %s WHERE id = %s
		""", (data['floor'], floor_id ) )

	DB.execute("COMMIT;")
	return data



def DELETE(id):
	'''
		DELETE is used to delete records.
	'''

	rows = DB.query("""SELECT building_id, floor_id FROM location WHERE id = %s""", ( int(id),) )
	bldg_id = rows[0][0]	
	floor_id = rows[0][1]

	DB.execute("BEGIN;")
	DB.execute("""DELETE FROM customer_locations WHERE location_id = %s""", (int(id), ))
	DB.execute("""DELETE FROM location WHERE id = %s""", (int(id), ))

	rows = DB.query("""SELECT count(*) FROM room WHERE floor_id = %s""", (floor_id, ))
	if rows[0][0] == 0:
		rows = DB.query("""SELECT count(*) FROM location WHERE floor_id = %s AND building_id = %s""", (floor_id, bldg_id) )
		if rows[0][0] == 0:
			DB.execute("""DELETE FROM floor WHERE id = %s""", (floor_id, ))

	rows = DB.query("""SELECT count(*) FROM floor WHERE building_id = %s""", (bldg_id, ))
	if rows[0][0] == 0:
		rows = DB.query("""SELECT count(*) FROM location WHERE building_id = %s""", (bldg_id, ) )
		if rows[0][0] == 0:
			DB.execute("""DELETE FROM building WHERE id = %s""", (bldg_id, ))
	DB.execute("COMMIT;")
	return {"message": "location deleted"}


"""
import cherrypy
import edc.db as DB

def POST():
	'''
		This API is not RESTful, but we'll use POST to CREATE new records, We'll return
		the ID's to the caller
	'''
	



def PUT():
	'''
		This API is not RESTful but we'll use PUT to UPDATE existing 
	'''


def DELETE():
	'''
		DELETE is used to delete records.
	'''
"""
