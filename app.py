import cherrypy
import os
from edc.log import Log
import edc.db as DB
from edc.config import Config, ConfigItem
from common.mapper import DbExtMap, TableRelMap
from common.security import Permission
import simplejson as json


class Root(object):
    @cherrypy.expose
    def index(self):
        '''
            This is needed to allow for / requests to deliver the index file
        '''
        with open(os.path.join(os.path.abspath(os.getcwd()),'index.html'), 'r') as f:
            buff = f.read()
            return buff

class BaseAPI(object):
    exposed = True

    def _cp_dispatch(self, vpath):
        Log.debug( "dispatch:  %s" % str(vpath) )

        return self

    def pathToModule(self, path):
        """
            This method tries to convert the path of the request URL 
            downstream of the /api/ portion into a python module path
            Which is then loaded and the HTTP.Method function if it exists
            is returned to the caller

        """
        methods = ('OPTIONS','GET','HEAD','POST',
                       'PUT','DELETE','TRACE','CONNECT')
        request = cherrypy.request

        if request.method not in methods: 
            raise cherrypy.HTTPError(400,'Bad Request') 

        # If request method is HEAD, return the page handler 
        # for GET, and let CherryPy take care of dropping  
        # the response body 
        method = request.method 
        if request.method == "HEAD": 
            method = "GET" 

        try:
            modPath = cherrypy.request.path_info[1:].replace('/', '.')
            Log.debug( "Trying to import: %s" % modPath )
            rv =  __import__(modPath)
            '''
                Deal with non-__all__ modules in packages
            '''
            for sub in modPath.split('.')[1:]:
                rv = getattr(rv, sub)

            #if hasattr(rv, modPath.split('.')[-1]):
            #    rv = getattr(rv, modPath.split('.')[-1])


            Log.debug( "Imported %s made up of %s" % (modPath, str(dir(rv))) )
            # check to see if this module implements this method
            if not getattr(rv, method):
                cherrypy.response.headers['Allow'] = list(set(methods).intersection(set(dir(rv))))
                raise cherrypy.HTTPError(405)
            
            return rv

        except Exception as e:
            Log.warn( "No handler for path: %s : [%s]" % (modPath, str(e)) )
            raise cherrypy.HTTPError(501,'Not Implemented')

    def handleRequest(self, **kwargs):
        """
            This method gets the controller module as specified by the 
            URL, and executes the handler, returning it's response
            (Which is converted into JSON by the default handler below)
        """
        Log.notice( "RestAPI requested to handle a [%s] to: %s" % (cherrypy.request.method, cherrypy.request.path_info) )
        controller = self.pathToModule(cherrypy.request.path_info)
        method = cherrypy.request.method 
        if cherrypy.request.method == "HEAD": 
            method = "GET" 
        if controller is not None:
            handler = getattr(controller, method, None)
            return handler(**kwargs)

    def tokenValid(self):
        if 'rtd_auth_token' in cherrypy.session:
            Log.info( "Session has token: %s" % str(cherrypy.session['rtd_auth_token']) )
            rows = DB.query("SELECT * FROM portal.session WHERE token = %s", (str(cherrypy.session['rtd_auth_token']),))
            if len(rows) > 0:
                return True
        return False

class RestAPI(BaseAPI):
    @cherrypy.expose
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def default(self, **kwargs):
        """
            Nearly all API requests will come through this function.

            So, check to see if this is a login attempt, if so, continue
            If not, check for an rtd_auth_token.

            If that is missing, redirect back to the main page

            If it does exist, try to handle the request.
        """
        Log.debug("Server recv request for: {}".format(cherrypy.request.path_info))
        try:
            if Permission.denied():
                Log.info("Permission denied to {}".format(cherrypy.request.path_info))
                raise cherrypy.HTTPError(401, "Permission Denied")
            else:
                return self.handleRequest(**kwargs)

        except Exception as e:
            DB.execute("ROLLBACK;")
            raise e


class RawAPI(BaseAPI):
    @cherrypy.expose
    def default(self, **kwargs):
        """
            Requests that do not want JSON responses should come through this handler.
        """
        Log.debug("Server recv request for: {}".format(cherrypy.request.path_info))
        try:
            if self.tokenValid():
                Log.debug("attempting to transform path into module...")
                return self.handleRequest(**kwargs)
            else:
                Log.debug("Invalid session detected")
                raise cherrypy.HTTPError(401, "Invalid Session")
        except Exception as e:
            raise e

def jsonify_error(status, message, traceback, version):
    response = cherrypy.response
    response.headers['Content-Type'] = 'application/json'
    Log.error("Wrapping error v{} - Status: {} message: '{}', traceback: \n{}".format(version, status, message, traceback))
    return json.dumps({'status': status, 'message': message, 'traceback': traceback})


if __name__ == '__main__':

    cfgitems = [
        ConfigItem(group='email', name='sender', typetype=str, mandatory=True, longname='email-sender', helptext="The email address from which portal generated emails will be sent")
        , ConfigItem(group='email', name='server', typetype=str, mandatory=True, longname='email-server', helptext="The FQDN or IP address of the SMTP server which will relay the email messages")
        , ConfigItem(group='email', name='login', typetype=str, mandatory=True, longname='email-login', helptext="The username with which to log into the outbound mail server")
        , ConfigItem(group='email', name='password', typetype=str, mandatory=True, longname='email-password', helptext='The password for email-login')
        , ConfigItem(group='http', name='baseurl', typetype=str, mandatory=True, longname='http-baseurl', helptext='The base URL this server will be available upon')
        , ConfigItem(group='allocation', name='shaperurls', typetype=str, mandatory=True, longname='shaperurls', helptext='The list of urls to apply traffic shaping')
        , ConfigItem(group='allocation', name='routers', typetype=str, mandatory=True, longname='routers', helptext='The list of router names/IP addresses to connect to')
        , ConfigItem(group='allocation', name='routerUsers', typetype=str, mandatory=True, longname='routerUsers', helptext='The list of router usernames to connect as')
        , ConfigItem(group='allocation', name='routerPWs', typetype=str, mandatory=True, longname='routerPWs', helptext='The list of router passwords to connect with')
        , ConfigItem(group='fax', name='directory', typetype=str, mandatory=False, longname='fax-directory', helptext='The directory structure where customer faxes can be found')

    ]
    
    config = Config(topHelp ="""
    app.py serves the API and static content for the RooftopData Portal
    """)

    cherrypy.tree.mount(Root(), '/', {'/': {
            'tools.staticdir.on': True
            , 'tools.staticdir.dir': './'
            , 'tools.staticdir.root': os.path.abspath(os.getcwd())
        }
    })

    cherrypy.tree.mount(RestAPI(), '/api', { '/' : {
            'tools.sessions.on': True
            , 'error_page.default': jsonify_error
            #, 'request.dispatch': cherrypy.dispatch.MethodDispatcher()
        }
    })

    cherrypy.tree.mount(RawAPI(), '/raw', { '/' : {
            'tools.sessions.on': True
            , 'error_page.default': jsonify_error
            #, 'request.dispatch': cherrypy.dispatch.MethodDispatcher()
        }
    })

    DB.init(config.username.get(), config.password.get(), config.hostname.get(), config.database.get(), connPerQuery=False, useInstance=True)
    Log.setVerbosity(config.get('settings.verbosity'))
    cherrypy.server.socket_host = '0.0.0.0'
    cherrypy.engine.start()
    cherrypy.engine.block()
