import edc.db as DB
import cherrypy
import simplejson as json

def PUT():
	data = cherrypy.request.json
	"""
		First of all, delete any expired enrollment tokens....
	"""
	DB.execute("DELETE FROM portal.enrollment WHERE now() - created_ts > '1 day'::interval;")
	if 'token' in data:
		rows = DB.query("SELECT email_addr FROM portal.enrollment AS e JOIN customer_contact_email AS c ON e.email_id = c.id WHERE e.token = %s", (data['token'],))
		if len(rows) > 0:
			return { "email": rows[0][0] }
	cherrypy.response.status = 401
	return { "message": "invalid enrollment" }





def POST():
	data = cherrypy.request.json
	"""
		First of all, delete any expired enrollment tokens....
	"""
	DB.execute("DELETE FROM portal.enrollment WHERE now() - created_ts > '1 day'::interval;")



	if 'password' in data and 'email' in data and 'token' in data:
		"""
			If there is a valid enrollment token

			UPSERT the password (as md5) for the user (email address)
			in the portal.users table
		"""
		rows = DB.query("""
				SELECT cc.customer_id 
				FROM portal.enrollment AS e 
				JOIN customer_contact_email AS c 
				ON e.email_id = c.id 
				JOIN customer_contact AS cc
				ON c.contact_id = cc.id
				WHERE e.token = %s 
				AND lower(c.email_addr) = lower(%s)
			""", (data['token'], data['email']) )

		if len(rows) > 0:
			"""
				There is a matching enrollment request

				Now, check if there is already a user with this id
				if so, update, else insert, then remove the enrollment token
			"""
			rows = DB.query("SELECT 1 FROM portal.users WHERE username = lower(%s)", (data['email'], ) )
			if len(rows) > 0:
				#update
				DB.execute("UPDATE portal.users SET password = md5(%s) where username = lower(%s)", (data['password'], data['email']) )
			else:
				#insert
				DB.execute("INSERT INTO portal.users (username, password, cust_id) VALUES (lower(%s), md5(%s), %s)", (data['email'], data['password'], rows[0][0]) )


			DB.execute("DELETE FROM portal.enrollment WHERE token = %s", (data['token'], ) )
			return { "message": "password established" }

	cherrypy.response.status = 401
	return { "message": "failed to set password" }