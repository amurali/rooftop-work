import cherrypy

def PUT():
	if 'rtd_auth_token' in cherrypy.session:
		del cherrypy.session['rtd_auth_token']

	cherrypy.lib.sessions.expire()
	return {"message": "session logged out"}
