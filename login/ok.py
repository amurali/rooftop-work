import cherrypy

def PUT():
	if 'rtd_auth_token' in cherrypy.session:
		token = cherrypy.session['rtd_auth_token']
	else:
		token = 'MISSING'
	print "Session ID: {}, token value: {}".format(cherrypy.session.id, token)
	if token != 'MISSING':
		return {"message": "session still valid"}
	cherrypy.response.status = 401
	return {"message": "login required"}