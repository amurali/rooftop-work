import cherrypy
from uuid import uuid4
import edc.db as DB
from edc.log import Log

def GET():
    print("Login.GET called")
    pass

def POST():
    print("login.login.POST called")
    data = cherrypy.request.json

    if 'username' in data and 'password' in data:
        rows = DB.query("""
                SELECT u.superuser, u.cust_admin, u.cust_id, c.name, c.full_name 
                FROM portal.users AS u
                JOIN secured_customer AS c
                ON u.cust_id = c.id
                WHERE username = lower(%s)
                AND password = md5(%s)
            """, (data['username'], data['password'] ))

        if len(rows) > 0:
            cherrypy.session['rtd_auth_token'] = uuid4()
            cherrypy.session['username']       = data['username']
            cherrypy.session['superuser']      = rows[0][0]
            cherrypy.session['cust_admin']     = rows[0][1]
            cherrypy.session['cust_id']        = rows[0][2]
            cherrypy.session['cust_abbr']      = rows[0][3]
            cherrypy.session['cust_name']      = rows[0][4]
            cherrypy.session['screens']        = {}

            """


                Need a dynamic means of creating the IN list
                for identifier matching. Need to detect and include any roles 
                the user might have assigned. For instance 
                there need to be cust_admin, and superuser roles
                as well as any not yet idintified roles somehow linked to 
                the username, or perhaps even customer Id



            """
            rows = DB.query("""
                        SELECT rolename
                        FROM portal.user_roles
                        WHERE username = %s;
                    """, (data['username'],) )

            rolelist = "'" + "', '".join([r[0] for r in rows]) + "'"

            Log.info("Fetching screens for user: {} with roles: ({})".format( data['username'], rolelist ))

            rows = DB.query("""
                SELECT ref, identifier
                FROM portal.screens
                WHERE identifier IN ({}, 'ALL')
                """.format(rolelist) )

            for r in rows:
                cherrypy.session['screens'][r[0]] = r[1]


            DB.execute("INSERT INTO portal.session (token, session_id, username) VALUES (%s, %s, %s)", (str(cherrypy.session['rtd_auth_token']), str(cherrypy.session.id), data['username']) )
            return { 'message': 'login successful', 'session_id': cherrypy.session.id }

    cherrypy.lib.sessions.expire()
    cherrypy.response.status = 401
    return { 'message': 'Not authorized' }

