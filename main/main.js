import { app } from '../common/app.js';
import FullHierarchicalEditable from '../mixins/FullHierarchicalEditable.js';
import riot from 'riot';
import observable from 'riot-observable';
import route from 'riot-route';
import $ from 'jquery';

// include the tags 
import "../admin/ipaddr/networks.riot"
import "../admin/bandwidth/allocator.riot"
import "../admin/sales/agents.riot"
import "../admin/users/users.riot"
import "../sales/enrollagent.riot"
import "../common/messages.riot"
import "../common/message.riot"
import "../common/typesel.riot";
import "../common/modal.riot"
import "../login/login.riot"
import "../main/main.riot"
import "../main/menu.riot"
import "../main/screen.riot"
import "../common/drow.riot"
import "../common/flatitem.riot"
import "../common/flatlist.riot"
import "../common/addbtn.riot"
import "../common/dobj.riot"
import "../common/df.riot"
import "../common/db.riot"
import "../common/dn.riot"
import "../common/dsel.riot"
import "../common/dmoney.riot"
import "../common/griditem.riot"
import "../common/gridlist.riot"
import "../summary/summary.riot"
import "../details/phone.riot"
import "../details/phones.riot"
import "../details/address.riot"
import "../details/addresses.riot"
import "../details/email.riot"
import "../details/emails.riot"
import "../details/contact.riot"
import "../details/contacts.riot"
import "../details/location.riot"
import "../details/locations.riot"
import "../details/details.riot"
import "../products/products.riot"
import "../products/services.riot"
import "../usage/usage.riot"
import "../billing/billing.riot"
import "../phone/phone.riot"
import "../fax/fax.riot"
import "../support/support.riot"

//app.FullHierarchicalEditable = FullHierarchicalEditable();
var appMixin =
{
  init: function() {
    this.app = app;

  },
  ansByName: function(name) {
    var chkParent = function(p)
    {
      if ( p && p.__ && p.__.tagName && p.__.tagName == name )
        return p;
      else if ( p && p.parent )
        return chkParent(p.parent);
      else
        return undefined;
    }
    return chkParent(this.parent);
  }

}

//riot.compile(function() {
  riot.mixin(appMixin);
  riot.mount('*');

//})

route(function(section, arg1, arg2, arg3, arg4) {
  app.trigger('route_'+ section, arg1, arg2, arg3, arg4);
});

route.start();
