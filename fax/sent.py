import cherrypy
import edc.db as DB
from edc.log import Log
from edc.config import GlobalConfig as cfg
import sys
import os
from datetime import datetime

def getCustId():
	return (cherrypy.session['cust_id'], )

def getCustAbbr():
	return cherrypy.session['cust_abbr']


def GET(number=None):
	"""
		returns a list of faxes received by this number (for this customer)
	"""
	if number is None or len(number) != 10:
		return cherrypy.HTTPError("Invalid fax number provided")

	try:
		cd = os.path.join( cfg.get('fax.directory'), getCustAbbr() )
		dr = os.path.join(cd, 'Sent')
		
		#dr = os.path.join(rd, "to_1{}".format(number) )

		Log.debug("Fetching sent faxes for number: {} of customer {} within: {}".format(number, getCustAbbr(), dr ))

 		rv = []
		for root, dirs, files in os.walk(dr):
			for file in files:
				if file.endswith(".pdf"):
					try:
						mtime = os.path.getmtime(os.path.join(root, file))
					except OSError:
						mtime = 0
					date = datetime.fromtimestamp(mtime).isoformat()

					recip = root.split('to_1')[1].split(os.path.sep)[0]

					rv.append({
						 "fullpath": os.path.join(root, file).replace(cd, '')
						, "fname": file
						, "remote": recip
						, "date": date
						, "mtime": mtime	
					})

		# return the list with most recent first in the list
		return sorted(rv, key=lambda f: -1*f['mtime'] )
	except Exception as e:
		last_type, last_value, exc_traceback = sys.exc_info()
		Log.tb(exc_traceback, last_type, last_value)
		raise e
