import cherrypy
import edc.db as DB
from edc.log import Log
from edc.config import GlobalConfig as cfg
import sys
import os

def getCustId():
	return (cherrypy.session['cust_id'], )

def getCustAbbr():
	return cherrypy.session['cust_abbr']


def GET(path=None):
	"""
		Return to the requester the file (in the current customer's fax folder)

		This needs to only be accessed by the RawAPI (/raw route) rather than the RestAPI (/api route)
		as trying to convert the file output to JSON will get ugly in a hurry
	"""
	if path is None:
		return cherrypy.HTTPError("Invalid file requested")

	try:
		cd = os.path.join( cfg.get('fax.directory'), getCustAbbr() )
		Log.debug("creating path: {} + {} == {}".format(cfg.get('fax.directory'), getCustAbbr(), cd ))
		if path[0] == os.path.sep:
			path = path[1:]
		fn = os.path.join(cd, path)

		Log.debug("Sending fax file {} of customer {}".format(fn, getCustAbbr() ))

		if fn.endswith('.pdf'):
			cherrypy.response.headers['Content-Type'] = 'application/pdf'
			cherrypy.response.headers['Content-Disposition'] = 'attachment; filename=fax.pdf'
		elif fn.endswith('.tiff'):
			cherrypy.response.headers['Content-Type'] = 'image/tiff'
			cherrypy.response.headers['Content-Disposition'] = 'attachment; filename=fax.tiff'
		else:
			return cherrypy.HTTPError(500, 'Unknown file type requested')

		cherrypy.serving.request.handler = None
		
		with open(fn, 'r') as f:
			buff = f.read()
			return buff

	except Exception as e:
		last_type, last_value, exc_traceback = sys.exc_info()
		Log.tb(exc_traceback, last_type, last_value)
		raise e