import cherrypy
import edc.db as DB
from edc.log import Log
from edc.config import GlobalConfig as cfg
import sys

def getCustId():
	return (cherrypy.session['cust_id'], )

numberMap = [
	{ "dbname": "id",      "pos": 0, "extname": "id",      "type": long, "pk": True }
,	{ "dbname": "cust_id", "pos": 1, "extname": None,      "type": long, "restrict": getCustId, "insert": getCustId, "update": getCustId }
,   { "dbname": "did",     "pos": 2, "extname": "number",  "type": str  }
]

def GET():
	try:
		m_numbers 	= TableRelMap("asterisk.customer_fax", numberMap)

		data = m_numbers.fetchAll()
		return data
	except Exception as e:
		last_type, last_value, exc_traceback = sys.exc_info()
		Log.tb(exc_traceback, last_type, last_value)
		raise e