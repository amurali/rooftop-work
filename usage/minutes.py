import cherrypy
import edc.db as DB
from edc.log import Log
import sys

def getCustAbbr():
	return (cherrypy.session['cust_abbr'], )

g_numMonths = 3

def getFirstDate():
	global g_numMonths
	"""
		g_numMonths presents a SQL injection vulnerability. It must be an int
	"""
	if type(g_numMonths) is not int:
		raise "Invalid number of months specified"

	return "first_dom >= now() - '{} months'::interval".format(g_numMonths)

"""
                    View "asterisk.cdr_billing_summary"
    Column    |         Type          | Modifiers | Storage  | Description 
--------------+-----------------------+-----------+----------+-------------
 accountcode  | character varying(20) |           | extended | 
 month        | double precision      |           | plain    | 
 year         | double precision      |           | plain    | 
 calls        | numeric               |           | main     | 
 bill_min     | numeric               |           | main     | 
 duration_min | numeric               |           | main     | 
 first_dom    | timestamp             |           | main     |
"""

monthlyMap = [
  {"dbname": "calls", 		"pos": 0, "extname": "calls", 	"type": int }
, {"dbname": "bill_min", 	"pos": 1, "extname": "minutes", "type": float }
, {"dbname": "accountcode", "pos": 2, "extname": None, 		"type": str, "restrict": getCustAbbr }
, {"dbname": "first_dom", 	"pos": 3, "extname": "date",	"type": str, "restrict": getFirstDate, "ordBy": 0 }

]



def GET(numMonths=None):
	try:
		m_monthly 		= TableRelMap("asterisk.cdr_billing_summary", 	monthlyMap, mode="bulkfetch")

		if numMonths is not None:
			global g_numMonths
			g_numMonths = int(numMonths)

		data = m_monthly.fetchAll()
		return data
	except Exception as e:
		last_type, last_value, exc_traceback = sys.exc_info()
		Log.tb(exc_traceback, last_type, last_value)
		raise e
