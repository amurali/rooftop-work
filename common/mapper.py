import edc.db as DB
from edc.log import Log
import datetime

#in python3, this was renamed to 'builtins'
import builtins

class DbExtMap(object):
    def __init__(self, mapping, mode="Singleton"):
        self.map = {} 
        self.positions = {}
        self.extnames = {}
        self.pkeys = []
        self.dependentVals = {}
        self.mode = mode
        self.paths = []
        self.retKeys = []

        for t in mapping:
            d = t['dbname']
            p = t['pos']
            e = t['extname']
            
            if "pk" in t:
                self.pkeys.append( t )

            if "parentFname" in t:
                self.dependentVals[ t['parentFname'] ] = t

            if "path" in t:
                self.paths.append( t['path'] )

            if "retOnIns" in t:
                self.retKeys.append( t )

            if d is not None:
                self.map[ d ] = t
            
            if p is not None:
                self.positions[ p ] = t
            
            if e is not None:
                self.extnames[ e ] = t

        if mode == "Singleton" and len(self.pkeys) == 0:
            raise Exception("Singleton mapping requested, but no primary key elements defined")

    def get_mapping_proj(self, mapping, useAlias=True):
        """
            renders the alias and column name and returns that string
        """
        if "path" in mapping:
            """
                grab the last alias in the path
            """
            alias = mapping["path"][len(mapping["path"]) - 1]["alias"]

        else:
            alias = "P__M__T__"

        if useAlias:
            return "{}.{}".format(alias, mapping['dbname'])
        else:
            return mapping['dbname']

    def sql_projection(self):
        """
            Return the names of the columns in "pos" order 
            so the data can be fetched, mapped and returned according to the mapping 
            specification
        """
        rv = None
        for i in sorted( [self.positions[n] for n in self.positions], key=lambda x: x["pos"] ):
            if rv is None:
                rv = self.get_mapping_proj(i)
            else:
                rv = "{}, {}".format(rv, self.get_mapping_proj(i))

        return rv

    def sql_projection_pk(self):
        """
            Return the primary key column(s) in pos "order"
        """
        rv = None
        for i in sorted( [ self.positions[n] for n in self.positions if "pk" in self.positions[n].keys() ], key=lambda x: x["pos"] ):
            if rv is None:
                rv = self.get_mapping_proj(i)
            else:
                rv = "{}, {}".format(rv, self.get_mapping_proj(i))
        if rv is None:
            Log.warn("Primary Key projection impossible due to incorrect mapping: " %  str(self.positions) )
            raise Exception("Requested Primary Key where none provided in mapping!!")
        return rv

    def getOrderBy(self):
        """
            Produce the order by clause of the query as defined by the ordBy values
            in the mapping
        """
        rv = ""
        for i in sorted( [ self.positions[n] for n in self.positions if "ordBy" in self.positions[n].keys() ], key=lambda x: x["ordBy"] ):
            if rv == "":
                rv = "ORDER BY {}".format( self.get_mapping_proj(i) )
            else:
                rv = "{}, {}".format(rv, self.get_mapping_proj(i) )

        return rv

    def getMoreWhere(self, sql, parms, conj=" AND ", useAlias=True):
        """
            Produce a additional expressions for the where clause
            according to the restrict values in the mapping
            The restriction value can be: 
                - a hard-coded value (as a single element tuple)
                - a string - presumed to be valid SQL
                - a function which returns a value
                - a function which returns an array of tuples defining:
                  - column name
                  - comparison operator
                  - values or more functions adhering to this paradigm
        """
        for mm in self.map:
            m = self.map[mm]
            if 'restrict' in m.keys():
                c = m['restrict']
                sql, parms = self.moreWhereParms(c, m, sql, parms, conj=conj, useAlias=useAlias)
                if conj == ' WHERE ':
                    conj = ' AND '

        return sql, parms

    def moreWhereParms(self, c, m, sql, parms, conj, useAlias=True):
        return self.moreParms(c,m,sql,parms,conj, " ",useAlias=useAlias)

    def moreSetParms(self, c, m, sql, parms, useAlias=True):
        return self.moreParms(c,m,sql,parms,", ", "",useAlias=useAlias)

    def moreParms(self, c, m, sql, parms, conj, conjAlt, oper="=", useAlias=True):

        val = None
        if '__call__' in dir(c):
            """
                this is a function, so, we'll call it, and see what it returns
                and deal with that accordingly
            """
            val = c()
        else:
            val = c

        """
            Now interrogate val to see it is a singleton, a tuple, or an array of either
        """
        if type(val) != list:
            val = [val]

        conj = conj
        if sql == "" or sql is None:
            conj = conjAlt

        for v in val:
            if type(v) == str:
                sql = "{}{}{}".format(sql, conj, v)
            elif type(v) == m['type'] and m['extname'] is None:
                sql = "{}{} {} {} {}".format(sql, conj, self.get_mapping_proj(m, useAlias), oper, v)
            elif type(v) == tuple and len(v) == 1:
                sql = "{}{}{} {} %s".format(sql, conj, self.get_mapping_proj(m, useAlias), oper)
                parms += v
            elif type(v) == tuple and len(v) == 3:
                sql = "{}{}{} {} %s".format(sql, conj, v[0], v[1])
                sql, parms = self.moreParms(v[2], {"dbname": v[0]}, sql, parms, conj, conjAlt, oper=v[1])
            else:
                Log.warn("Skipping invalid mapping: %s %s => %s" % (str(type(v)), str(v), str(m)))

        return sql, parms

    def moreValParms(self, c, m, names, values, parms):
        """
            This is only called when building insert statements.
            But, it's job is to handle the different parameters of a mapping and 
            build the correct components of the clauses to facilitate the insert
        """
        val = None
        if '__call__' in dir(c):
            """
                this is a function, so, we'll call it, and see what it returns
                and deal with that accordingly
            """
            val = c()
        else:
            val = c

        """
            Now interrogate val to see it is a singleton, a tuple, or an array of either
        """
        if type(val) != list:
            val = [val]

        if names is None:
            names = ""
        if values is None:
            values = ""

        for v in val:
            if type(v) == tuple and len(v) == 1:
                if names is None or names == "":
                    names = m['dbname']
                else:
                    names = "{}, {}".format(names, m['dbname']) # don't use the get_mapping_proj function as aliases are not supported in inserts

                if values is None or values == "":
                    values = "%s"
                else:
                    values = "{}, %s".format(values)
                parms += v
            elif type(v) == tuple and len(v) == 3:
                if names is None or names == "":
                    names = v[0]
                else:
                    names = "{}, {}".format(names, v[0] )

                if values is None or values == "":
                    values = "%s"
                else:
                    values = "{}, %s".format(values)

                parms += v[2]
            else:
                Log.warn("Skipping invalid mapping: %s" % str(m))

        return names, values, parms


    def rowToExt(self, r):
        """
            Create a dictionary made up of the projected data renamed according to the mapping
        """
        rv = {}
        for i in sorted( [self.positions[n] for n in self.positions], key=lambda x: x["pos"] ):
            Log.debug( "trying to assign the %d val from %s into %s " % (i["pos"], str(r), i["extname"]) )

            """
                shoehorn the database data into the mapping requested data type....
            """
            if type(r[i['pos']]) is not i['type'] and r[i['pos']] is not None:
                if type(r[i['pos']]) is datetime.datetime:
                    """
                        dates need to be converted to ISO strings
                    """
                    val = r[i['pos']].isoformat()
                else:
                    """
                        Just try a brute force casting for everything else
                    """
                    val = i['type'](r[i['pos']])
            else:
                val = r[i['pos']]

            if i['extname'] is not None:
                rv[ i['extname'] ] = val
            
            if rv is None:
                """
                    Not sure what this is expected to do....
                """
                #rv = i["dbname"]
                pass
        return rv

    def pkQuery(self):
        """
            Return the primary key portion of the where clause 
        """
        qry = None

        for k in self.pkeys:
            if qry is None:
                qry = "{} = %s".format( self.get_mapping_proj(k) )
            else:
                qry = " AND {} = %s".format( qry, self.get_mapping_proj(k) )

        return qry

    def getPkData(self, dat):
        """
            Return the tuple of the primary key from the provided data (in external representation)
        """
        args = ()

        for k in self.pkeys:
            if k['extname'] in dat:
                args += (dat[ k['extname'] ], )

        return args

    def pkForQuery(self, dat):
        """
            Return the primary key portion of the where clause 
            and the matching parameters to the query to be passed 
            to the database call
        """
        qry = self.pkQuery()
        args = self.getPkData(dat)
        return qry, args

    def fkQuery(self):
        """
            Return the foreign key portion of the where clause 
        """
        qry = None
        for ky in self.dependentVals.keys():
            k = self.dependentVals[ky]
            if qry is None:
                qry = "{} = %s".format( self.get_mapping_proj(k) )
            else:
                qry = " AND {} = %s".format( qry, self.get_mapping_proj(k) )

        return qry

    def getFkData(self, dat):
        """
            Return the foreign key matching parameters to be passed 
            to the database call
        """
        args = ()

        for ky in self.dependentVals.keys():
            k = self.dependentVals[ky]
            if k['extname'] in dat:
                args += (dat[ k['extname'] ], )

        return args


    def fkForQuery(self, dat):
        """
            Return the foreign key portion of the where clause 
            and the matching parameters to the query to be passed 
            to the database call
        """
        qry = self.fkQuery()
        args = self.getFkData(dat)

        return qry, args


    def datForUpdate(self, dat):
        """
            Generate and return the SET portion of the update statement, and
            the values to be passed as parameters to the database query call
        """
        qry = None
        args = ()
        Log.debug( "Aligning mapping to update for data:\n\n {}".format(str(dat)) )
        for kk in self.map:
            k = self.map[kk]
            if ('pk' not in k or not k['pk']) and k['extname'] in dat and 'path' not in k:
                """
                    skip primary keys as they are to be returned.
                    Ensure there is a mappable element in the externally provided data
                    Do not update mappings with paths, as those columns come from other tables.
                """
                if qry is None:
                    qry = "{} = %s".format(k['dbname'])
                else:
                    qry = "{}, {} = %s".format( qry, k['dbname'] )

                args += ( dat[ k['extname'] ], )
                Log.debug( "Mapping value: {} element: {}".format( dat[ k['extname'] ], str(k) ) )

            if k['extname'] not in dat:
                Log.info( "NOT Mapping element: {}".format( str(k) ) )

            if 'update' in k:
                qry, args = self.moreSetParms(k['update'], k, qry, args, useAlias=False)

        return qry, args




    def datForInsert(self, dat):
        """
            Generate and return the column names, values and returning portions (if there are pk's)
            of the insert statement, and the parameters to the database query call
        """
        nams = None
        vals = None
        sfx = None
        args = ()
        Log.debug( "Aligning mapping to insert for data:\n\n {}".format(str(dat)) )
        for kk in self.map:
            k = self.map[kk]
            if ('pk' not in k or not k['pk'] or 'pkInsertable' in k) and k['extname'] in dat and 'path' not in k and 'retOnIns' not in k:
                """
                    skip primary keys as they are to be returned. Also skip retOnIns columns for the same reason
                    Ensure there is a mappable element in the externally provided data
                    Do not insert mappings with paths, as those columns come from other tables.
                """
                if nams is None:
                    nams = k['dbname']
                    vals = "%s"
                else:
                    nams = "{}, {}".format(nams, k['dbname'])
                    vals = "{}, %s".format(vals)

                args += ( dat[ k['extname'] ], )
                Log.debug( "Mapping value: {} element: {}".format( dat[ k['extname'] ], str(k) ) )

            if k['extname'] not in dat:
                Log.info( "NOT Mapping element: {}".format( str(k) ) )

            if 'insert' in k:
                nams, vals, args = self.moreValParms(k['insert'], k, nams, vals, args)

        for k in self.pkeys:
            if 'pkInsertable' not in k:
                if sfx is None:
                    sfx = "RETURNING {}".format( k['dbname'] )
                else:
                    sfx = "{}, {}".format( sfx, k['dbname'] )

        for k in self.retKeys:
            if sfx is None:
                sfx = "RETURNING {}".format( k['dbname'] )
            else:
                sfx = "{}, {}".format( sfx, k['dbname'] )

        return nams, vals, sfx, args

    def pksInDat(self, dat):
        """
            This checks to see if the primary keys are sufficiently 
            represented such that queries will be appropriately restricted
        """
        rv = True
        for k in self.pkeys:
            if k['extname'] not in dat:
                rv = False

        if len(self.pkeys) == 0:
            rv = False

        return rv


    def delExtFromTable(self, tab, dat):
        """
            This function will delete matching data from the table,
            only if the primary keys are accounted for
        """
        if len(self.pkeys) > 0:
            where, parms = self.pkForQuery(dat)
            where, parms = self.getMoreWhere(where, parms, useAlias=False)
            fromC = self.getFromClause(tab, nopath=True)
            if len(parms) >= len(self.pkeys):
                qry = "DELETE FROM {} WHERE {}".format(fromC, where)

                Log.info("Running: %s, with %s" % (qry, str(parms)))
                DB.execute(qry, parms)
            else:
                raise Exception("When deleting data, the primary key values must be supplied to the API")
        else:
            raise Exception("Deleting data via a mapping is only possible when the mapping defines the primary key")

    def delExtFromTableByParent(self, tab, dat):
        """
            This function will delete matching data from the table
        """
        if len(self.pkeys) > 0:
            where, parms = self.fkForQuery(dat)
            where, parms = self.getMoreWhere(where, parms, useAlias=False)
            fromC = self.getFromClause(tab, nopath=True)
            qry = "DELETE FROM {} WHERE {}".format(fromC, where)
            if len(parms) > 0:
                Log.info("Running: %s, with %s" % (qry, str(parms)))
                DB.execute(qry, parms)
            else:
                Log.error("Not Running: %s, with %s" % (qry, str(parms)))
                raise Exception("When deleting data by foreign key, the key values must be provided")
        else:
            raise Exception("Deleting data via a mapping is only possible when the mapping defines the primary key")

    def fetchIdsFromTable(self, tab):
        """
            This function returns the array of array's of the primary key column values from the table
        """
        if len(self.pkeys) > 0:
            cols = self.sql_projection_pk()
            fromC = self.getFromClause(tab)
            qry = "SELECT {} FROM {}".format(cols, fromC)
            qry, parms = self.getMoreWhere(qry, (), conj=" WHERE ")
            Log.info("Running: %s with %s" % (qry, parms))
            return DB.query(qry, parms)

        else:
            raise Exception("Tried to fetch all the ID's from a table without primary key's specified")

    def getFromClause(self, tab, nopath=False):
        """
            This function builds the from clause to include any joins specified by path entries in the 
            mapping
        """
        rv = "{} AS P__M__T__".format(tab)
        if nopath:
            return rv

        for path in self.paths:
            priorAlias = "P__M__T__"
            for p in path:
                joinType = "JOIN"
                if "jmethod" in p:
                    joinType = p['jmethod']

                if type(p['fk']) == str:
                    rv = " {} {} {} AS {} ON {}.{} = {}.{}".format(rv, joinType, p['table'], p['alias'], p['alias'], p['pk'], priorAlias, p['fk'])
                elif type(p['fk']) in [list,tuple,set] and type(p['pk']) in [list,tuple,set] and len(p['fk']) == len(p['pk']):
                    raise Exception('Multi-column keys not implemented in paths')
                else:
                    raise Exception('Unexpected path definition in mapping. Invalid mapping definition')
                priorAlias = p['alias']

        return rv

    def fetchExtFromTable(self, tab, parms):
        """
            This function assembles a returnable dictionary according to the mapping
        """
        if len(self.pkeys) > 0:
            if len(parms) == len(self.pkeys):
                where = self.pkQuery()
            else:
                raise Exception("When selecting data, the primary key values must be supplied to the API")
        elif self.mode == 'bulkfetch':
            where = "1=1"
        else:
            raise Exception("Selecting data via a mapping is only possible when the mapping defines the primary key or mode is bulkfetch")

        proj = self.sql_projection()
        ordby = self.getOrderBy()
        where, parms = self.getMoreWhere(where, parms)
        fromC = self.getFromClause(tab)
        qry = "SELECT {} FROM {} WHERE {} {}".format(proj, fromC, where, ordby)
        Log.info("Running: %s, with %s" % (qry, str(parms)))
        rows = DB.query(qry, parms)

        if len(rows) == 1:
            return self.rowToExt(rows[0])
        elif len(rows) > 1:
            rv = list()
            for r in rows:
                rv.append(self.rowToExt(r))
            return rv
        else:
            return None


    def fetchExtFromTableByFk(self, tab, parms):
        """
            This function assembles a returnable dictionary or array according to the mapping
            by querying against a foreign key
        """
        if len(self.dependentVals) > 0:
            if len(parms) == len(self.dependentVals):
                where = self.fkQuery()
                proj = self.sql_projection()
                ordby = self.getOrderBy()
                where, parms = self.getMoreWhere(where, parms)
                fromC = self.getFromClause(tab)
                qry = "SELECT {} FROM {} WHERE {} {}".format(proj, fromC, where, ordby)
                Log.info("Running: %s, with %s" % (qry, str(parms)))
                rows = DB.query(qry, parms)

                if len(rows) == 1:
                    return [self.rowToExt(rows[0])]
                elif len(rows) > 1:
                    rv = list()
                    for r in rows:
                        rv.append(self.rowToExt(r))
                    return rv
                else:
                    return None
            else:
                raise Exception("When selecting data, the primary key values must be supplied to the API")
        else:
            raise Exception("Selecting data via a mapping is only possible when the mapping defines the primary key")


    def applyExtToTable(self, tab, dat):
        """
            This function will insert or update data into the specified table
            by checking:
                - is the PK field in the submitted data
                - if so, update
                - if not, insert
            In either case, return the latest data from the DB
        """
        doInsert = True
        if len(self.pkeys) > 0:
            where, parms = self.pkForQuery(dat)
            fromC = self.getFromClause(tab)
            if len(parms) == len(self.pkeys):
                qry = "SELECT COUNT(*) FROM {} WHERE {}".format(fromC, where)
                rows = DB.query(qry, parms)
                if rows[0][0] == 1:
                    """
                        This is an update
                    """
                    update, vals = self.datForUpdate(dat)
                    fromC = self.getFromClause(tab,nopath=True)
                    qry = "UPDATE {} SET {} WHERE {}".format(fromC, update, where)
    
                    Log.info("Running: %s, with %s" % (qry, str(vals + parms)))

                    DB.execute(qry, vals + parms)
                    doInsert = False
                elif rows[0][0] > 1:
                    """
                        This is an error (trying to UPDATE will affect more than one row)
                    """
                    raise Exception("Mapping has a primary key defined, and an update was detected, but, the update will affect multiple rows, so aborting")


        """
            Raise an exceptions in singleton mode, otherwise,
            just try to cram this into the database via appending
        """
        if self.mode == "Singleton" and len(self.pkeys) == 0:
            raise Exception("Mapping missing Primary Key Data for table: %s" % tab )

        """
            Otherwise, we are clear to insert the provided data, if needed
        """
        if doInsert:
            names, values, suffix, args = self.datForInsert(dat)
            qry = "INSERT INTO {} ({}) VALUES ({}) {}".format(tab, names, values, suffix)

            Log.info("Running: %s, with %s" % (qry, str(args)))
            rows = DB.query(qry, args)
            if len(rows) == 1:
                """
                    It seems there was a RETURNING result, so stick these values
                    into the data object passed in's PK fields
                """
                i = 0
                for k in self.pkeys:
                    dat[ k['extname'] ] = rows[0][i]
                    i+=1

    def prepDependentDat(self, data, parentData):
        """
            Populates the fields in data with the parent objects data in parentFname field
        """
        for k in self.dependentVals.keys():
            dv = self.dependentVals[k]
            Log.debug("Setting FK: %s = %s" % (dv['dbname'], parentData[k]))
            #data[ dv['dbname'] ] = parentData[k]
            data[ dv['extname'] ] = parentData[k]








class TableRelMap(object):
    def __init__(self, tab, mapDef, parent=None, parent_fname=None, mode="singleton"):
        self.tab = tab
        self.map = DbExtMap(mapDef, mode=mode)
        self.mode = mode
        self.parent = parent
        self.parentFname = parent_fname # this is the name of the field in the main dat that the parent can use to link with this map
        self.children = {}
        self.workData = None

        if self.parent is not None:
            self.parent.children[parent_fname] = self





    def apply(self, data):
        """
            Applies the parent data, then, for each child data present in the 
            data, prepares the data for query submission by populating any children
            then applies each record for each child
        """
        Log.debug("Applying changes in data to: %s" % self.tab)
        if type(data) == list:
            trash = list()
            for d in data:
                if "__trash" in d and self.map.pksInDat(d):
                    trash.append(d)

                    self.delete(d)
                elif "__trash" not in d:
                    self.map.applyExtToTable(self.tab, d)
                else:
                    """
                        trash data sent to API (this is expected)
                        And, it has no primary key data, which indicates it has never been saved
                        so, don't do anything with it
                    """
                    Log.debug("Ignoring external data as it is trash: %s" % str(d))
                    trash.append(d)

            for t in trash:
                Log.info( "Removing element from data for table %s " % self.tab )
                data.remove(t)
        else:
            if "__trash" in data and self.map.pksInDat(data):

                self.delete(data)
                Log.info( "Trying to delete an object: %s" % str(data) )
                keys = data.keys()
                for k in keys:
                    del data[k]
            elif "__trash" not in data:
                self.map.applyExtToTable(self.tab, data)
            else:
                """
                    trash data sent to API (this is expected)
                    And, it has no primary key data, which indicates it has never been saved
                    so, don't do anything with it
                """
                Log.debug("Ignoring external data as it is trash: %s" % str(data))
                keys = data.keys()
                for k in keys:
                    del data[k]

        for k in self.children.keys():
            child = self.children[k]
            if k in data:
                if type(data[k]) == list:
                    for d in data[k]:
                        child.map.prepDependentDat(d, data)
                        child.apply(d)
                else:
                    child.map.prepDependentDat(data[k], data)
                    child.apply(data[k])







    def delete(self, data):
        """
            Traverses the mapping and data to delete all dependent records prior to the parent records
        
            First, traverse data to the deepest anscestor to the current data
            Then, returning up the stack, delete all the children, with the fk data == to the parents pk data
        """
        Log.info( "Trying to delete from %s with: %s" % (self.tab, str(data)) )

        for k in self.children.keys():
            Log.debug( "     deleting from child %s first" % (k) )
            child = self.children[k]
            if k in data and data[k] is not None:
                if type(data[k]) == list:
                    for d in data[k]:
                        child.map.prepDependentDat(d, data)
                        child.delete(d)
                else:
                    child.map.prepDependentDat(data[k], data)
                    child.delete(data[k])


        Log.debug( "     NOW deleting from table: %s" % (self.tab) )
        if self.parent is not None and not self.map.pksInDat(data):
            self.map.delExtFromTableByParent(self.tab, data)
        else:
            self.map.delExtFromTable(self.tab, data)






    def fetch(self, idval):
        """
            Query all the data according to the mapping and return the 
            appropriately structure of dictonaries and lists
        """
        if len(idval) == 0:
            raise Exception("Fetch called with no primary key data")

        Log.debug("Fetching row with id: %s from %s" % (str(idval), self.tab))
        if self.parent is None:
            data = self.map.fetchExtFromTable(self.tab, idval)
        else:
            data = self.map.fetchExtFromTableByFk(self.tab, idval )

        if data is not None:
            if type(data) is list:
                for dat in data:
                    for k in self.children.keys():
                        child = self.children[k]
                        pkd = self.map.getPkData(dat)
                        if len(pkd) > 0:
                            dat[k] = child.fetch(  pkd )
                        else:
                            Log.debug("Not fetching any data from child: %s" % child.tab)
            else:
                for k in self.children.keys():
                    child = self.children[k]
                    pkd = self.map.getPkData(data)
                    if len(pkd) > 0:
                        data[k] = child.fetch(  pkd )
                    else:
                        Log.debug("Not fetching any data from child: %s" % child.tab)


        return data



    def fetchAll(self): 
        """
            Grab all id values in the table, and, for each child, get the related data
        """
        rv = []
        data = []
        if self.mode == 'singleton':
            for r in self.map.fetchIdsFromTable(self.tab):
                Log.debug( "Fetching from %s for ID: %s" % (self.tab, str(r)) )
                data = self.fetch(r)
                rv.append(data)
        elif self.mode == 'bulkfetch':
            rv = self.map.fetchExtFromTable(self.tab, () )
        return rv


'''
    This is done like this because I want to maintain this directory structure and not move 
    everything into one more subdirectory just so I can relative import these classes
'''
builtins.DbExtMap = DbExtMap
builtins.TableRelMap = TableRelMap