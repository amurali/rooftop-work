import cherrypy
from edc.log import Log
import edc.db as DB

class Permission(object):

    @staticmethod
    def denied():
        """
            This method interrogates the environment to determine
            if the request should be allowed or denied
        """
        if cherrypy.request.path_info in [ '/login/login', '/login/logout', '/login/ok', '/login/register', '/login/enroll', '/common/link', '/common/session', '/summary/summary' ]:
            Log.info("Permission granted due to whitelisted URL")
            return False

        rows = DB.query("""
            SELECT 1
            FROM portal.permissions
            WHERE identifier = 'ALL'
            AND path = %s
            AND method = %s
            """, ( cherrypy.request.path_info, cherrypy.request.method ))
        if len(rows) > 0:
            Log.info("Permission granted due to public permissions")
            return False


        if 'rtd_auth_token' in cherrypy.session:
            rows = DB.query("""SELECT token
                                , session_id
                                , username
                                , when_ts
                                FROM portal.session
                                WHERE token = %s""", (str(cherrypy.session['rtd_auth_token']),))
            if len(rows) > 0:
                Log.info( "Session has valid token: %s" % str(cherrypy.session['rtd_auth_token']) )

                token    = rows[0][0]
                sesid    = rows[0][1]
                username = rows[0][2]
                when_ts  = rows[0][3]

                rows = DB.query("""SELECT superuser
                                   , cust_admin
                                   , cust_id
                                   FROM portal.users
                                   WHERE username = %s
                        """, (username, ) )
                if len(rows) > 0:
                    superuser  = rows[0][0]
                    cust_admin = rows[0][1]
                    cust_id    = rows[0][2]

                if superuser:
                    Log.info("Permission granted due to superuser")
                    return False

                if cust_admin and cherrypy.request.path_info in []:
                    rows = DB.query("""
                        SELECT 1
                        FROM portal.permissions
                        WHERE identifier = 'cust_admin'
                        AND path = %s
                        AND method = %s
                        """, ( cherrypy.request.path_info, cherrypy.request.method ))
                    if len(rows) > 0:
                        Log.info("Permission granted due to customer admin")
                        return False

                rows = DB.query("""
                    SELECT 1
                    FROM portal.permissions
                    WHERE identifier = 'USER'
                    AND path = %s
                    AND method = %s
                    """, ( cherrypy.request.path_info, cherrypy.request.method ))
                if len(rows) > 0:
                    Log.info("Permission granted due to all users permissions")
                    return False


                """
                    Now, search for an alignment between portal.security and session
                """
                rows = DB.query("""
                    SELECT 1
                    FROM portal.permissions
                    WHERE identifier = %s
                    AND path = %s
                    AND method = %s
                    """, ( username, cherrypy.request.path_info, cherrypy.request.method ))
                if len(rows) > 0:
                    Log.info("Permission explicitly granted to user")
                    return False

        Log.notice("Access to {} - {} denied".format(cherrypy.request.method, cherrypy.request.path_info))
        return True



