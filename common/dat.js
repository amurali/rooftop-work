/*
  The Dat object gives us a history of changes 
  to any simple object, and the ability to select which 
  revision is the one to work with.

  The premise is an initial value is set for each member. This initial 
  value is never lost. A working copy of each field is established. Changes made to the 
  working copies are volatile until the whole object is commited. Committing establishes a 
  new working copy of all the member fields
  and the prior working copies are now members of the history
*/

var Arr = (function() 
{   
  function fsa() 
  {    
    var arrProto = Object.create( Array.prototype );    
    arrProto = ( Array.apply( arrProto, arguments ) || arrProto );    
    fsa.injectClassMethods( arrProto );     

    // set up instance variables .... I hope....
    arrProto.__changed =  false;
    arrProto.__trash =  [];
    arrProto.__IsDat =  true;
    arrProto.__parentDat =  undefined;
    arrProto.__datMember =  undefined;


    return ( arrProto );  
  }     
  fsa.injectClassMethods = function( inFsa )  
  {     
    for ( var method in fsa.prototype )     
    {       
      if ( fsa.prototype.hasOwnProperty( method ) )       
      {         
        inFsa[ method ] = fsa.prototype[ method ];      
      }     
    }     
    return ( inFsa );   
  }; 

  fsa.prototype = {
    changing: function() 
    {
      if ( this.__parentDat )
        this.__parentDat.handleArrChange(this, this.__datMember);
      this.__changed = true;
    },
    elementChanging: function()
    {
      this.__changed = true;
    },
    setDatParent: function(dat, name)
    {
      this.__parentDat = dat;
      this.__datMember = name;
    },
    getParentDat: function()
    {
      if ( this.__parentDat )
      {
        var rv = this.__parentDat.getParentDat();
        return rv || this.__parentDat;
      }
      return null;
    },
    concat: function()
    {
      this.changing();
      alert("Array concatination not fully implemented....")
      var rv = Array.prototype.concat.call(this);
      return rv;
    },

    pop: function()
    {
      this.changing();
      var rv = Array.prototype.pop.call(this);
      if ( this.__trash.indexOf( rv ) < 0 )
        this.__trash.push( rv )
      return rv;
    },

    push: function(v)
    {
      this.changing();
      var t = new_Dat(v);
      if ( t.setParentArray )
        t.setParentArray(this);
      var rv = Array.prototype.push.call(this, t);
      return rv;
    },

    shift: function()
    {
      this.changing();
      var rv = Array.prototype.shift.call(this);
      if ( this.__trash.indexOf( rv ) < 0 )
        this.__trash.push( rv )
      return rv;
    },

    // we may not correctly handle splicing to add elements....
    splice: function(i, l)
    {
      this.changing();
      var rv = Array.prototype.splice.call(this, i, l);
      for ( var n in rv )
      {
        if ( this.__trash.indexOf( rv[n] ) < 0 )
          this.__trash.push( rv[n] )
      }
      return rv;
    },

    unshift: function(v)
    {
      this.changing();
      var t = new_Dat(v);
      if ( t.setParentArray )
        t.setParentArray(this);
      var rv = Array.prototype.unshift.call(this, t);
      return rv;
    },
    join: function(sep)
    {
      var rv = "";
      for ( var i = 0; i < this.length; i++ )
      {
        if ( i > 0 )
        {
          rv += sep;
        }
        rv += this[i].toString();
      }
      return rv;
    },
    deepCopy: function()
    {
      var rv = new Arr();
      for ( var i = 0; i < this.length; i++ )
      {
        var t = this[i];
        if ( t.__IsDat === undefined )
        {
          t = new_Dat(t);
          t.setParentArray(this);
        }  
        rv.push(t)
      }
      rv.__changed = false;
      return rv;
    },
    fromArray: function(a)
    {
      for ( var i in a )
      {
        if ( a[i].__IsDat === undefined )
        {
          var t = new_Dat(a[i]);
          if ( t.setParentArray )
            t.setParentArray(this);
          Array.prototype.push.call(this, t);
        }
        else
          Array.prototype.push.call(this, a[i]);
      }
    },
    toString: function()
    {
      var rv = "";
      for ( var i = 0; i < this.length; i++ )
      {
        if ( i > 0)
          rv += ", ";
        rv += this[i].toString();
      }
      return rv;
    },
    toObject: function()
    {
      var rv = [];
      for ( var i = 0; i < this.length; i++ )
      {
        var r = this[i];
        if ( r.__IsDat )
          r = r.toObject();
        rv.push(r);
      }


      /*
        The Trash will be emptied by the API before any updates are applied.
        This means that if a user deletes an entry from an array, but, goes back in 
        history to where the deleted item exists and saves it again, the item will be 
        re-created by the API in the database. (Likely a new ID will be assigned). This should propogate 
        back through the API.

        Similarly
      */

      if ( this.__trash.length > 0)
      {
        rv.__trash = [];
        for ( var i = 0; i < this.__trash.length; i++)
        {
          var r = this.__trash[i];
          if ( r.__IsDat )
            r = r.toObject();
          r.__trash = "DELETEME";
          rv.push( r );
        }
      }
      return rv;
    }
  }

  return ( fsa );   
}).call( {} ); 

function setGetterSetter(me, k)
{
  Object.defineProperty(me, k, {
    get: function()
    {
      //console.log("Dat member requested: " + k)
      var rv = me.__work[k];
      return rv; 
    },
    set: function(v) 
    {
      if ( v && typeof v == "object" && v.length !== undefined )
      {
        if ( v.__IsDat === undefined )
        {
          var tv = new Arr();
          tv.fromArray(v)
          v = tv;
        }
      }
      if ( v && v.__IsDat === undefined && typeof v == "object" )
      {
        v = new_Dat(v);
      }

      if ( me.__data[k] === undefined )
      {
        me.__data[k] = v;
      }
      if ( me.__InParent )
      {
        me.__InParent.changing();
        me.commit();
      }
      me.__work[k] = v;

    }
    , configurable: true
    , enumerable: true
  });            

}

function addValueToMe(me, val, k)
{
  if ( k[0] == '_')
  {
    console.log("Dat wrap hidden member named: " + k)
    me[k] = val[k];
    return this;
  }
  me.__changes[k] = [];

  setGetterSetter(me, k);  

  if (val[k] && typeof val[k] == "object" && val[k].length !== undefined)
  {
    /*
    ** this is an array
    -- arrays are handled as follows
      - Each commit where there are changes
        within the array will cause there to be a new copy of the array
      - Each commited change of the parent object will hold a reference 
        to the array as of that change (this could be the new copy too)
      - When elements are deleted from the array, they are saved in 
        a arrayTrash array, which holds the unique
        items removed from the child array
      - undo's and redo's will just traverse the history of array references and copies
      - reverts will create a new array copy based on the original
      - If the elements of the array are objects
        They will be treated as objects, and deep-copied when the array is changed
    */
    me.__data[k] = new Arr();
    me.__data[k].fromArray(val[k]);
    var tmp = new Arr()
    me.__work[k] = tmp;
    me.__work[k].fromArray(val[k]);
    me.__changes[k][0] = me.__work[k];
    var changes = me.__changes[k];
    me.__types.push("Array");
    me.__work[k].setDatParent(me, k);
  }
  else if ( val[k] && typeof val[k] == "object" )
  {
    // this is an object
    me.__data[k] = new_Dat(val[k]);
    me.__work[k] = me.__data[k];
    me.__changes[k][0] = me.__data[k];
    me.__types.push("Object");
  }
  else if ( typeof val == "object" && k in val )
  {
    // this will be the case when val is an object and we are just assigning a value from within it
    me.__data[k] = val[k];
    me.__work[k] = val[k];
    me.__changes[k][0] = val[k];
    me.__types.push("Singleton");
  }
  else
  {
    // we'll end up here when val is a singleton as well
    me.__data[k] = val;
    me.__work[k] = val;
    me.__changes[k][0] = val;
    me.__types.push("Singleton");
  }
  me.__keys.push(k);      
  //console.log("Dat object added member type: " + me.__types[me.__types.length - 1])


}

function Dat(val)
{
  var me = this;
  if ( val && val.__IsDat !== undefined )
  {
    //console.log("Attempted to create a new Dat from an existing one...")
    return val.deepCopy();
  }
  // basic types of objects are to be left alone
  if ( typeof val != "object" )
  {
    alert("Implementation Error!!!! Tried to DAT Wrap a singleton value");
  }

  this.__data       = {};
  this.__work       = {};
  this.__changes    = {};
  this.__currentIdx = 0;
  this.__keys       = [];
  this.__types      = []
  this.__IsDat      = true;
  this.__InParent   = undefined;

  for ( var idx in Object.keys(val))
  {
    (function () {
      var k = Object.keys(val)[idx];

      addValueToMe(me, val, k);

    })();
  }
}

Dat.prototype.setParentArray = function(arr)
{
  this.__InParent = arr;
}

Dat.prototype.getParentDat = function()
{
  if ( this.__InParent )
  {
    var rv = this.__InParent.getParentDat();
    //if ( rv )
    //  console.log("Returning alternate Dat: ", rv);
    return rv || this.__InParent;
  }
  return null;
}

// fetch a hidden/masked data element (one prefixed with _)
Dat.prototype.getHidden = function(nm)
{
  if ( this[nm] )
      return this[nm];
  else
    return undefined;
}

// given an input object, overwrite current field values with the 
// fields with the same name in the input object
// if the field is not known, add it to this object
// do so recursively traversing matching sub objects and arrays
Dat.prototype.updateFrom = function(f)
{
  if ( typeof f == 'object' && f.length === undefined )
  {
    for ( var k in f )
    {
      var idx = this.__keys.indexOf(k);
      if ( idx > -1 && k[0] !== '_') // don't overwrite "hidden" fields
      {

        var t = this.__types[ idx ];
        var v =  this[k];
        switch ( t )
        {
          case "Array"     : console.error("updateFrom for array not implemented"); break;
          case "Object"    : v.updateFrom(f[k]); break;
          case "Singleton" : this[k] = f[k]; break;
        }
      }
      else
      {
        // this field is not presently set in this object
        // add it
        console.log("Adding previously unknown field: " + k + " = " + f[k]);
        addValueToMe(this, f[k], k);
      }
    }
  }
}

// return an fresh-non-historied copy of this instances current values
Dat.prototype.deepCopy = function()
{

  var wrk = new_Dat({});

  wrk.__data       = {};
  wrk.__work       = {};
  wrk.__changes    = {};
  wrk.__currentIdx = 0;
  wrk.__keys       = [];
  wrk.__types      = []
  wrk.__IsDat      = true;
  wrk.__InParent   = undefined;

  for ( var i in this.__keys )
  {
    var k = this.__keys[i];
    var t = this.__types[i];
    (function() {
      setGetterSetter(wrk, k);
    })();
    wrk.__keys.push(k);
    wrk.__types.push(t);
    wrk.__changes[k] = [];
    wrk[k] = this[k];
    switch (t) {
      case "Array"     : wrk.__changes[k][0] = wrk.__work[k]; break;
      case "Object"    : wrk.__changes[k][0] = wrk.__data[k]; break;
      case "Singleton" : wrk.__changes[k][0] = this[k]; break;
    }
  }
  return wrk;
}

/*
  This function basically tells the array to operate on a new 
  copy of the array instead of the instance the change 
  was requested upon.
*/
Dat.prototype.handleArrChange = function(tmp, k) {
  if ( ! tmp.__changed )
  {
    // only for the first change before a commit
    // create an unmodified copy, and set the currentIdx to that
    var t2 = tmp.deepCopy();
    this.__changes[k][this.__currentIdx] = t2;
    t2.__changed = true;
    //this.__work[k] = t2;
  }
}

Dat.prototype.commit = function()
{
  // first check to see if anything is different
  var hasChanges = false;
  for ( var i in this.__keys )
  {
    var k = this.__keys[i];
    switch( this.__types[i] )
    {
      case "Singleton":
        if ( this.__changes[k][this.__currentIdx] != this.__work[k] )
        {
          hasChanges = true;
        }
      break;
      case "Object":
        console.error("Dat.commit change detection on sub-objects NOT IMPLEMENTED");
      break;
      case "Array":
        if ( this.__changes[k][this.__currentIdx].__changed )
        {
          console.log("Changes detected in child array");
          hasChanges = true;
        }
      break;
    }
  }
  if ( hasChanges )
  {
    // if there are, add them to the end of the arrays
    for ( var i in this.__keys )
    {
      var k = this.__keys[i];


      switch( this.__types[i] )
      {
        case "Singleton":
          this.__changes[k][this.__changes[k].length] = this.__work[k];
        break;
        case "Object":
          console.error("Dat.commit on sub-objects NOT IMPLEMENTED");
        break;
        case "Array":
          if ( this.__changes[k][this.__currentIdx].__changed )
          {
            // this array is changed, set work to a new deep copy
            this.__work[k].__changed = false;
            this.__changes[k][this.__currentIdx].__changed = false;
            //this.__work[k] = this.__work[k].deepCopy();
          }
          this.__changes[k][this.__changes[k].length] = this.__work[k];
        break;
      }


    }
    this.__currentIdx = this.__changes[this.__keys[0]].length - 1;

  }
}

Dat.prototype.revert = function()
{
  for ( var i in this.__keys )
  {
    var k = this.__keys[i];
    this.__changes[k][this.__changes[k].length] = this.__data[k];
  }
  this.__currentIdx++;
  
}

Dat.prototype.undo = function()
{
  this.__currentIdx = (this.__currentIdx > 0) ? this.__currentIdx - 1 : 0;
  for ( var i in this.__keys )
  {
    var k = this.__keys[i];
//    if ( this.__types[k] == "Array")
//      this.__work[k].undo();
    this.__work[k] = this.__changes[k][this.__currentIdx];
  }

}

Dat.prototype.redo = function()
{
  this.__currentIdx = (this.__currentIdx < this.__changes[this.__keys[0]].length - 1) ? this.__currentIdx + 1 : this.__changes[this.__keys[0]].length - 1;
  for ( var i in this.__keys )
  {
    var k = this.__keys[i];
//    if ( this.__types[k] == "Array")
//      this.__work[k].redo();
    this.__work[k] = this.__changes[k][this.__currentIdx];
  }
}

Dat.prototype.canUndo = function()
{
  return (this.__currentIdx > 0 )
}

Dat.prototype.canRedo = function()
{
  return (this.__currentIdx < (this.__changes[this.__keys[0]].length - 1))
}

Dat.prototype.numUndo = function ()
{
  return this.__currentIdx;
}

Dat.prototype.numRedo = function ()
{
  return this.__changes[this.__keys[0]].length - this.__currentIdx - 1;
}

Dat.prototype.toObject = function()
{
  var rv = {};
  for ( var i in this.__keys )
  {
    var k = this.__keys[i];
    if ( this.__work[k] && this.__work[k].__IsDat !== undefined )
    {
      rv[k] = this.__work[k].toObject()
    }
    else
    {
      rv[k] = this.__work[k];
    }
  }
  return rv;
}

Dat.prototype.toString = function()
{

  for ( var i in this.__keys )
  {
    var k = this.__keys[i];
    var t = this.__types[i];
    switch (t) {
      case "Array"     : return wrk.__data[k].toString(); break;
      case "Object"    : return wrk.__data[k].toString(); break;
      case "Singleton" : return this[k]; break;
    }
  }
}

function new_Dat(v) {
  if ( typeof v == "object" )
  {
    return new Dat(v);
  }
  else
  {
    return v;
  }
}

export default Dat;
