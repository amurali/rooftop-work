import riot from 'riot';
import { ajax } from './ajax.js';
import observable from 'riot-observable';

function App()
{
  var me = this;
  this.route_args = {
      arg1: undefined
    , arg2: undefined
    , arg3: undefined
    , arg4: undefined 
    , route: undefined
  };

  this.data = {};
  this.alerts = [];
  this.alertView = null;

  this.session = {
      superuser: false
    , admin:     false
    , screens:   {}
  }
  // for locations dev work...
  this.data.locations = [];
  this.data.contacts = [];
  this.data.products = [];
  this.data.services = [];

  this._fetchTypes = [
    {"table": "customer_contact_type",   "varname": "contact_types"}
    , {"table": "customer_email_type",   "varname": "contact_email_types"}
    , {"table": "customer_phone_type",   "varname": "contact_phone_types"}
    , {"table": "customer_address_type", "varname": "contact_address_types"}
    , {"table": "product_type", "varname": "product_types"}
  ]

  // initialize the types tables to empty lists
  for ( var i in this._fetchTypes )
  {
    var t = this._fetchTypes[i];
    this.data[t.varname] = [];
  }

  this.changes = {};

  me.loggedIn = false;
  riot.observable(me);
}

App.prototype.registerMessage = function(msg)
{
  if ( this.alertView === null )
    this.alertView = msg;
}

App.prototype.showAlerts = function()
{
  return this.alerts.length > 0
}

App.prototype.addMessage = function(obj)
{
  this.alerts.unshift(obj);
  this.alertView.update();
}

App.prototype.handleAjaxErr = function(obj)
{
  // check to see if the message is a "401 Unauthorized" message
  // if so, redirect back to the login page rather than showing the message
  console.log("handleAjaxErr ");
  console.log(obj);
  if ( 'responseJSON' in obj )
  {
    if ( 'status' in obj.responseJSON && obj.responseJSON.status == '401 Unauthorized')
    {
      console.log("Expired session or just not authorized...let's go to the login screen");
      this.clearRouteArgs();
      this.trigger("loggedOut")
    }
    else if ( 'status' in obj.responseJSON && 'message' in obj.responseJSON && 'traceback' in obj.responseJSON )
    {
      this.addMessage({"message": "Status: " + obj.responseJSON.status + " - message: " + obj.responseJSON.message + " traceback: " + obj.responseJSON.traceback });
    }
    else if ( 'message' in obj.responseJSON )
    {
      this.addMessage(obj.responseJSON);
    }    
  }
  else if ( 'status' in obj && 'message' in obj && 'traceback' in obj )
  {
    this.addMessage({"message": "Status: " + obj.status + " - message: " + obj.message + " traceback: " + obj.traceback });
  }
  else if ( 'status' in obj && 'message' in obj )
  {
    this.addMessage({"message": "Status: " + obj.status + " - message: " + obj.message });
  }
  else if ( 'message' in obj )
  {
    this.addMessage({"message": obj.message });
  }
  else if ( 'responseText' in obj )
  {
    this.addMessage( { "message": obj.responseText } );
  }
  else
  {
    alert("Abnormal API response...");
    this.addMessage({"message": JSON.stringify(obj)});
  }
}

App.prototype.remMessage = function(obj)
{
  var idx = this.alerts.indexOf(obj)
  this.alerts.splice(idx, 1);
  this.alertView.update();
}

App.prototype.clearRouteArgs = function()
{
  this.route_args = {
      arg1: undefined
    , arg2: undefined
    , arg3: undefined
    , arg4: undefined 
    , route: undefined
  };        
}

App.prototype.setRouteArgs = function(evt, arg1, arg2, arg3, arg4)
{
  this.route_args = {
      arg1: arg1
    , arg2: arg2
    , arg3: arg3
    , arg4: arg4 
    , route: evt
  };        
}

App.prototype.logTypes = function()
{
  console.log("----------------------------------------------   BEGIN type listing ---------------------------------------");
  for ( var i in this._fetchTypes )
  {
    var t = this._fetchTypes[i];
    console.log("Type [" + t.varname + "] contains:");
    console.log(this.data[t.varname]);
  }
  console.log("----------------------------------------------   END type listing ---------------------------------------");
  console.log(" --");
}

App.prototype.loadTypes = function(cbk)
{
  var me = this;
  return new Promise(
    function(resolve, reject)
    {
      return me._fetchTypes.reduce(
        function(accumulator, val)
        {
          return accumulator.then(
            function()
            {
              return ajax.get("/api/common/types?name=" + val.table);
            }
          )
          .then(
            function(data)
            {
              me.data[val.varname] = data;
            }
          )
        }, Promise.resolve()
      )
      .then(function(data)
        {
          console.log("Type array reduction complete..." + typeof cbk)
          if ( cbk && typeof cbk == 'function')
          {
            console.log("Calling get session callback");
            return cbk();
          }
        }
      )
      ;
    }
  )
  ;

}

App.prototype.getSession = function(cbk)
{
  var me = this;
  ajax.get("/api/common/session").then(
    function(data)
    {
      console.log("Session promise fulfilled")
      me.session = data;
      me.loadTypes(cbk)
      .then(
        function() 
        {
          console.log("Types loaded")
        }
      )
      .catch(function() { me.addMessage( { "message": "Type loading failed" } ); });
    }
  )
  .catch(
    function(xhr)
    {
      me.addMessage({ "message": xhr.responseText });
    }
  );

}

var _app = new App();

export let app = _app;

