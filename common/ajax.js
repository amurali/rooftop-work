function Ajax() {};

function reqPromise(method, url, indata)
{
	// indata is optional
	return new Promise(

		function(resolve, reject) 
		{
			var xhr = new XMLHttpRequest();
			xhr.open(method, url);

			xhr.onload = function() {
				if ( this.status >= 200 && this.status < 300 )
				{
					if ( 'responseJSON' in this )
						resolve(this.responseJSON)
					try
					{
						resolve(JSON.parse(this.responseText))
					}
					catch(e)
					{
						xhr.responseText = "Exception " + e + " parsing: " + xhr.responseText; 
						reject(xhr);
					}
				}
				else
				{
					console.log("Recv'd Error response of type: " + xhr.getResponseHeader("Content-Type"))
					if ( xhr.getResponseHeader("Content-Type") == "application/json")
						reject(JSON.parse(this.responseText));
					else if ( 'responseJSON' in this )
						reject(this.responseJSON)
					else
						reject(xhr);
				}
			}
			xhr.onerror = function() 
			{
				reject(xhr);
			}

			if ( indata )
			{
				xhr.setRequestHeader("Content-type", "application/json");
				if ( typeof indata == 'string' )
					xhr.send( indata );
				else
					xhr.send( JSON.stringify(indata) );
			}
			else
			{
				xhr.send();
			}


		}
	);
}



Ajax.prototype.get = function(url, indata)
{
	return reqPromise("GET", url, indata);
}

Ajax.prototype.put = function(url, indata)
{
	return reqPromise("PUT", url, indata);
}


Ajax.prototype.post = function(url, indata)
{
	return reqPromise("POST", url, indata);
}

Ajax.prototype.delete = function(url, indata, func, error)
{
	return reqPromise("DELETE", url, indata);
}

var _ajax = new Ajax();

export let ajax = _ajax;
