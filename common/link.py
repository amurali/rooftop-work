import edc.db as DB
from edc.log import Log
import cherrypy
from uuid import uuid4
import simplejson as json

def GET(code=None): 
	"""
		This is a general purpose handler which allows un-authenticated 
		users to be granted tokens for subsequent API access
	"""


	if code is not None:
		rows = DB.query("""
				SELECT path, max_uses, times_used, data
				FROM portal.link_codes
				WHERE code = %s
				AND expires > now()
				AND ( times_used < max_uses OR max_uses IS NULL );
			""", ( code, ))

		if len(rows) > 0:
			path       = rows[0][0]
			max_uses   = rows[0][1]
			times_used = rows[0][2]
			data       = json.loads(rows[0][3])

			cherrypy.session['rtd_auth_token'] = uuid4()
			cherrypy.session['username']       = 'UNAUTH'
			cherrypy.session['superuser']      = False
			cherrypy.session['cust_admin']     = False
			cherrypy.session['cust_id']        = None
			cherrypy.session['cust_abbr']      = None
			cherrypy.session['cust_name']      = None
			cherrypy.session['link_data']      = data
			cherrypy.session['screens']        = {}

			rows = DB.query("""
				SELECT ref, identifier
				FROM portal.screens
				WHERE identifier IN ('UNAUTH', 'ALL')
				""" )

				for r in rows:
					cherrypy.session['screens'][r[0]] = r[1]


			Log.info("Code {} used {} times of {} to reach {}".format(code, times_used, max_uses, path))

			DB.execute("INSERT INTO portal.session (token, session_id, username) VALUES (%s, %s, %s)", (str(cherrypy.session['rtd_auth_token']), str(cherrypy.session.id), 'UNAUTH') )
			DB.execute("UPDATE portal.link_codes SET times_used = times_used + 1 WHERE code = %s", ( code, ) )

			if max_uses is not None and times_used + 1 >= max_uses:
				Log.notice("Code {} usage quota {} reached. Deleting".format(code, max_uses))
				DB.execute("DELETE FROM portal.link_codes WHERE code = %s;", ( code,) )

			raise cherrypy.HTTPRedirect(path)

	DB.execute("DELETE FROM portal.link_codes WHERE expires < now();", ( code,) )
	cherrypy.lib.sessions.expire()
	Log.info('request: {}'.format(str(dir(cherrypy._cprequest))))
	cherrypy._cprequest._cpconfig.set('error_page.default', page_return)
	raise cherrypy.HTTPError(401, "Invalid destination")
