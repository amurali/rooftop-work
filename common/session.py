import cherrypy

def GET():
	if 'rtd_auth_token' in cherrypy.session:
		rv = {
				  "token":     str(cherrypy.session['rtd_auth_token'])
				, "superuser": cherrypy.session['superuser']
				, "admin":     cherrypy.session['cust_admin']
				, "cust_id":   cherrypy.session['cust_id']
				, "cust_abbr": cherrypy.session['cust_abbr']
				, "cust_name": cherrypy.session['cust_name']
				, "screens":   cherrypy.session['screens']
		}

		if "data" in cherrypy.session:
			rv["data"] = cherrypy.session["data"]
		else:
			rv["data"] = {}
	else:
		rv = {
			  "token"      : None
			, "superuser"  : False
			, "admin"      : False
			, "cust_id"    : None
			, "cust_abbr"  : None
			, "cust_name"  : None
			, "data"       : {}
			, "screens"    : {}
		}

	return rv