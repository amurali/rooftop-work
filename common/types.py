import edc.db as DB
import cherrypy
import re

def GET(name): 
	# sanitize the name by making sure there are no semi-colons or spaces in it.
	# from there, the DB should just safely puke on malformed input.... (FLW)
	name = name.replace(' ', '').replace(';', '')
	rows = DB.query("""SELECT id, name FROM {} ORDER BY id""".format(name))
	rv = []
	for r in rows:
		rv.append({"id": r[0], "name": r[1]})
	return rv
